<?php

namespace FCS;

use Illuminate\Database\Eloquent\Model;

class ResponsabilidadAdministrativa extends Model
{
    protected $table = 'responsabilidad_administrativa';
    protected $fillable=[ 'id_profesor',
    					  'id_vinculacion',
    					  'id_periodo',
    					  'id_funcion',
    					  'horas_semanal'
    					  ]; 

     public function getPeriodo()
    {
        return $this->belongsTo('\FCS\Periodo','id_periodo');
    }

    public function getPeriodoCompletoAttribute(){
        $periodo=Periodo::find($this->attributes["id_periodo"]);
        return $periodo->anio." ".$periodo->periodo;
    }

    public function getFuncion()
    {
    	return $this->belongsTo('\FCS\FuncionAdministrativa','id_funcion');
    }

    public function getNombreProfesoresAttribute(){
        $profesores=Profesor::find($this->attributes["id_profesor"]);
        return $profesores->primer_nombre." ".$profesores->segundo_nombre." ".$profesores->primer_apellido." ".$profesores->segundo_apellido;
    }
    public function getNombreVinculacionAttribute(){
        $vinculacion=Vinculacion::find($this->attributes["id_vinculacion"]);
        return $vinculacion->nombre_vinculacion;
    }
}
