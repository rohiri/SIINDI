<?php

namespace FCS;

use Illuminate\Database\Eloquent\Model;

class FuncionAdministrativa extends Model
{
    protected $table = 'funciones_administrativas';
    protected $fillable=['nombre_funcion','sesion_consejo','fecha_sesion','max_horas'];

    public static function allLists()
    {
        return self::get()->lists('nombre_funcion' ,'id' );
    }
}
