<?php

namespace FCS;

use Illuminate\Database\Eloquent\Model;

class Profesor extends Model
{
    protected $table = 'profesores';
    protected $fillable=['cedula','primer_nombre','segundo_nombre','primer_apellido','segundo_apellido','email','telefono'];

    public static function allLists()
    {
        return self::get()->lists('cedula' ,'id' );
    }

    public function getNombreProfesoresAttribute(){
    	return $this->attributes["primer_nombre"]." ".$this->attributes["segundo_nombre"]." ".$this->attributes["primer_apellido"]." ".$this->attributes["segundo_apellido"];
    }

}

