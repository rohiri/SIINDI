<?php

namespace FCS;

use Illuminate\Database\Eloquent\Model;

class ResponsabilidadDocente extends Model
{
    protected $table = 'responsabilidad_docentes';
    protected $fillable=[ 'id_profesor',
    					  'id_grupo',
    					  'id_vinculacion',
    					  'numero_estudiantes',
    					  'horas_directas',
                          'horas_tutoria',
    					  'horas_preparacion',
    					  'numero_semanas',
    					  'total_horas'
    					  ]; 
    public function getGrupo()
    {
        return $this->belongsTo('\FCS\Grupo','id_grupo');
    }  
    					
    public function getProfesor()
    {
        return $this->belongsTo('\FCS\Profesor','id_profesor');
    } 
    
    public function getPeriodo()
    {
        return $this->belongsTo('\FCS\Periodo','id_periodo');
    } 
    
    public function getNombreVinculacionAttribute(){
        $vinculacion=Vinculacion::find($this->attributes["id_vinculacion"]);
        return $vinculacion->nombre_vinculacion;
    }

    public function getNombreProfesoresAttribute(){
        $profesores=Profesor::find($this->attributes["id_profesor"]);
        return $profesores->primer_nombre." ".$profesores->segundo_nombre." ".$profesores->primer_apellido." ".$profesores->segundo_apellido;
    }
}
