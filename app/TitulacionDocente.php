<?php

namespace FCS;

use Illuminate\Database\Eloquent\Model;

class TitulacionDocente extends Model
{
    protected $table = 'titulacion_docentes';
    protected $fillable=['id_profesor','id_programa','id_nivelestudio','titulo_estudio','id_periodo'];

    public function getProfesores()
    {
    	return $this->belongsTo('\FCS\Profesor','id_profesor');
    }

    public function getPrograma()
    {
    	return $this->belongsTo('\FCS\Programa','id_programa');
    }

    public function getNivel()
    {
    	return $this->belongsTo('\FCS\NivelEstudio','id_nivelestudio');
    }

    public function getPeriodo()
    {
    	return $this->belongsTo('\FCS\Periodo','id_periodo');
    }   

    
}
