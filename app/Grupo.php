<?php

namespace FCS;


use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    protected $table = "grupos";
    protected $primaryKey="id";
    protected $fillable = array("codigo","id_curso","id_periodo");

    public function getNombreCompletoAttribute(){
    	$curso=Curso::find($this->attributes["id_curso"]);
    	return $curso->nombre_curso." ".$curso->codigo_curso;
    }

    public function getPeriodoCompletoAttribute(){
        $periodo=Periodo::find($this->attributes["id_periodo"]);
        return $periodo->anio." ".$periodo->periodo;
    }
    public function getNombreCursoAttribute(){
    	$curso=Curso::find($this->attributes["id_curso"]);
    	return $curso->nombre_curso;
    }
    public function getNombreCodigoAttribute(){
    	$curso=Curso::find($this->attributes["id_curso"]);
    	return $curso->codigo_curso;
    }
    public function getNombreSemestreAttribute(){
    	$curso=Curso::find($this->attributes["id_curso"]);
    	return $curso->semestre;
    }

}