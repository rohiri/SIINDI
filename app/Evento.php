<?php

namespace FCS;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $table = 'eventos';
    protected $fillable=['numero_consejo','fecha','nombre_evento','descripcion_evento','lugar','caracter_evento','id_tipoeventos'];

    public function tipo()
    {
    	return $this->belongsTo('\FCS\TipoEvento','id_tipoeventos');
    }   

    public function getNombreEventoAttribute(){
    	return $this->attributes["nombre_evento"];
    }
}


