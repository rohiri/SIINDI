<?php

namespace FCS;

use Illuminate\Database\Eloquent\Model;

class Movilidad extends Model
{
    protected $table = 'movilidades';
    protected $fillable=['tipo_movilidad','id_profesor','id_estudiante','id_evento','id_periodo'];

    public function Profesor()
    {
    	return $this->belongsTo('\FCS\Profesor','id_profesor');
    }
    
    public function Periodo()
    {
    	return $this->belongsTo('\FCS\Periodo','id_periodo');
    }

    public function Evento()
    {
    	return $this->belongsTo('\FCS\Evento','id_evento');
    }  
}
