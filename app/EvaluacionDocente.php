<?php

namespace FCS;

use Illuminate\Database\Eloquent\Model;

class EvaluacionDocente extends Model
{
	protected $table = 'evaluacion_docentes';
    protected $fillable=['id_profesor','id_periodo','valor','nota'];
    
    public function getPeriodo()
    {
    	return $this->belongsTo('\FCS\Periodo','id_periodo');
    }  

    public function getProfesor()
    {
    	return $this->belongsTo('\FCS\Profesor','id_profesor');
    } 

}
