<?php

namespace FCS;


use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    protected $table = "estudiantes";
    protected $primaryKey="id";
    protected $fillable = array("primer_nombre" , "segundo_nombre" , "apellido_paterno" , "apellido_materno" ,  "numero_documento" , "codigo_estudiante" , "email");

	public function getNombreConCodigoAttribute(){
    	return $this->attributes["codigo_estudiante"]." - ".$this->attributes["apellido_paterno"]." ".$this->attributes["apellido_materno"]." ".$this->attributes["primer_nombre"]." ".$this->attributes["segundo_nombre"];
    }
}
