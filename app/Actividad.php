<?php

namespace FCS;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
    protected $table = 'actividades';
    protected $fillable=['nombre_actividad','sesion_consejo','fecha_sesion'];

    public static function allLists()
    {
        return self::get()->lists('nombre_actividad' ,'id' );
    }
}


