<?php

namespace FCS;

use Illuminate\Database\Eloquent\Model;

class ResponsabilidadOtras extends Model
{
    protected $table = 'otras_responsabilidades';
    protected $fillable=[ 'id_profesor',
    					  'id_vinculacion',
    					  'id_periodo',
    					  'id_actividad',
    					  'horas_semanal',
                          'fecha_inicio',
    					  'fecha_terminacion',
    					  'fecha_entregainforme'
    					  ]; 
     public function getPeriodo()
    {
        return $this->belongsTo('\FCS\Periodo','id_periodo');
    }

    public function getPeriodoCompletoAttribute(){
        $periodo=Periodo::find($this->attributes["id_periodo"]);
        return $periodo->anio." ".$periodo->periodo;
    }

    public function getActividad()
    {
        return $this->belongsTo('\FCS\Actividad','id_actividad');
    }

    public function getNombreProfesoresAttribute(){
        $profesores=Profesor::find($this->attributes["id_profesor"]);
        return $profesores->primer_nombre." ".$profesores->segundo_nombre." ".$profesores->primer_apellido." ".$profesores->segundo_apellido;
    }
    public function getNombreVinculacionAttribute(){
        $vinculacion=Vinculacion::find($this->attributes["id_vinculacion"]);
        return $vinculacion->nombre_vinculacion;
    }
}
