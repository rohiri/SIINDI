<?php

namespace FCS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;


class Permiso extends Model
{
    protected $table = 'permisos';
    protected $fillable=['iduser','idmenus','permiso'];

    public static function verificarPermiso($ruta){
        $menu=Menu::where("ruta","=",DB::raw("'".$ruta."'"))->first();
        $permiso=self::where("idmenus","=",DB::raw($menu->id))->where("iduser","=",DB::raw(Auth::user()->id))->first();
        if(!is_null($permiso))
            if($permiso->permiso==1)
                return true;
        return false;
    }

}
