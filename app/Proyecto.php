<?php

namespace FCS;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    protected $table = 'proyecto';
    protected $fillable=['codigo','nombre_proyecto','tipo','fecha_inicio','fecha_terminacion','sesion_consejo','fecha_sesion'];

    
    public static function allLists()
    {
        return self::get()->lists('nombre_proyecto' ,'id' );
    }
}


