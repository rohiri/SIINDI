<?php

namespace FCS;

use Illuminate\Database\Eloquent\Model;

class EstudiantesPreu extends Model
{
    protected $table = 'estudiantes_preu';
    protected $fillable=['id_periodo','e_inscritos','e_admintidos','matriculados_total',
    					 'matriculados_primersemestre','graduados','retirados','tasa_desercion',
    					 'tasa_culminacion'];

    public function getPeriodo(){
        return $this->belongsTo('\FCS\Periodo','id_periodo');
    }

    public function getNombrePeriodoAttribute(){
        $periodo= Periodo::find($this->attributes["id_periodo"]);
        return $periodo->anio." ".$periodo->periodo;
    }
}
