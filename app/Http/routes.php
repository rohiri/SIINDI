<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::controllers([
	'auth'		=> 'Auth\AuthController',
	'password'	=> 'Auth\PasswordController',
	
]);

Route::group([ 'middleware' => 'auth'], function(){

//--------------------RUTAS SECRETARIA------------------------------------------
	Route::get('/','HomeController@index');
	Route::get('/auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);
	Route::resource('usuario','usercontroller');
	Route::resource('tipo-evento','TipoEventoController');
	Route::resource('evento','EventoController');
	Route::resource('vinculacion','VinculacionController');
	Route::resource('programa','ProgramaController');
	Route::resource('nivel-estudio','NivelEstudioController');
	Route::resource('profesores','ProfesorController');
	Route::resource('planes-estudio','PlanEstudioController');
	Route::resource('cursos','CursoController');
	Route::resource('titulacion-docente','TitulacionController');
	Route::resource('evaluacion-docente','EvaluacionDocenteController');
	Route::resource('responsabilidad-docente','ResponsabilidadDocenteController');
	Route::post('responsabilidad-docente/importar','ResponsabilidadDocenteController@importar');
	Route::get('descargar_formato/{formato}','ResponsabilidadDocenteController@descargarFormato');
	Route::resource('dedicacion-docente','DedicacionDocenteController');
	Route::resource('permisos-secretaria','PermisosSecretariaController');
	Route::resource('secretaria-estudiante','EstudiantesPreuController');
	Route::resource('movilidad-docente','MovilidadController');
	Route::resource('funciones-administrativas','FuncionAdministrativaController');
	Route::resource('actividades','ActividadesController');
	route::get('/home','HomeController@home');
	Route::get('grafica/{id}','GraficasSecretariaController@grafJason');
	Route::get('grafica','GraficasSecretariaController@grafica');
//-------------------RUTAS PROGRAMA------------------------------------------
	Route::resource('estudiante','estudianteController');
    Route::get("json/usuario","datatablesController@getUsuarios");
    Route::get("preguntas","preguntasController@index");
    Route::post("preguntas/create","preguntasController@create");
    Route::post("preguntas/store","preguntasController@store");
    Route::resource('encuesta','encuestaController');
    Route::resource('grupo','grupoController');
    Route::resource('practicas','practicasController');
    Route::get('practicas/estudiantes/{id}','practicasController@estudiantes');
    Route::post('practicas/estudiantes/{id}','practicasController@estudiantesStore');
    Route::get('practicas/evaluar/{id}','practicasController@evaluar');
    Route::post('practicas/evaluar/{id}','practicasController@evaluarEstudiante');
    Route::post('practicas/evaluar/{id}/{estudiante}','practicasController@evaluarStore');
    Route::resource('inscripcion','InscripcionEstudianteCursoController');
    Route::resource('periodo','periodoController');
    Route::resource('rotacion','rotacionController');
    Route::resource('homologacion','homologacionController');
    Route::get('ajax/cargarRotaciones','ajaxController@cargarRotaciones');
    Route::get('graficas','graficasController@index');
    Route::get('graficar','graficasController@graficar');
});

Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
