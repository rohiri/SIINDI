<?php

namespace FCS\Http\Controllers;

use Illuminate\Http\Request;

use FCS\Periodo;

use FCS\Http\Requests;
use FCS\Http\Controllers\Controller;

class periodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *a
     * @return Response
     */
    public function index()
    {
        $periodos=Periodo::All();
        return view("formulario programa.Secretaria.Periodo.index",compact("periodos"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */    public function create()
    {
      
     $route = array('url' => 'periodo', 'method'=>'POST');
     $periodo=new \FCS\Periodo;
     return view('formulario programa.Secretaria.Periodo.Agregar_periodo', compact('route',"periodo"));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {        
       $data=\Input::all();
       $periodo=\FCS\Periodo::where("anio","=",\DB::raw($data["anio"]))->where("periodo","=",\DB::raw("'".$data["periodo"]."'"))->first();
       if(!is_null($periodo))
        return redirect("periodo/create")->with("mensaje-error","El periodo ya existe!")->withInput();
        \DB::transaction(function()use($data){
            $periodo=new \FCS\Periodo;
            $periodo->fill($data);
            $periodo->save();
        \FCS\Auditorias::registrar("PERIODO","INSERTAR",$periodo->id," ",json_encode($data));

        });
        return redirect("periodo")->with("mensaje","El periodo fue guardado!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
       $route = array('route' => array('periodo.update',$id), 'method'=>'PATCH');
     $periodo=\FCS\Periodo::find($id);
     return view('formulario programa.Secretaria.Periodo.Agregar_periodo', compact('route',"periodo")); 
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
   public function update(Request $request, $id)
    {
        $data=\Input::all();
        $periodo=\FCS\Periodo::where("anio","=",\DB::raw($data["anio"]))->where("periodo","=",\DB::raw("'".$data["periodo"]."'"))->first();
        if(!is_null($periodo))
            return redirect("periodo/".$id."/edit")->with("mensaje-error","El periodo ya existe!")->withInput();
        \DB::transaction(function()use($data,$id){
            $periodo=\FCS\Periodo::find($id);
            $periodo->fill($data);
            $periodo->save();
        });
        return redirect("periodo")->with("mensaje","El periodo fue actualizado!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        \DB::transaction(function()use($id){
            $periodo=\FCS\Periodo::find($id);
            $periodo->delete();
        });
        return redirect("periodo")->with("mensaje-error","El periodo fue eliminado!");
        
    }
}