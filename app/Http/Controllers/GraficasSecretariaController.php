<?php

namespace FCS\Http\Controllers;

use FCS\ResponsabilidadAdministrativa;
use FCS\ResponsabilidadDocente;
use FCS\ResponsabilidadInvestigacion;
use FCS\ResponsabilidadOtras;
use FCS\ResponsabilidadProyeccion;
use Illuminate\Http\Request;
use FCS\Http\Requests;
use FCS\Http\Controllers\Controller;

class GraficasSecretariaController extends Controller
{
    
    public function index()
    {
        return view('prueba');
    }

    public function grafica()
    {
        $periodos=\FCS\Periodo::orderby("anio","desc")->orderby("periodo","desc")->get()->lists("NombrePeriodo","id");
        return view ('grafica',compact('periodos'));
    }

    public function grafJason($id)
    {
        if($id==0){
            $datos1 = [];
            $datos2= [];
            $periodo=\Input::get("periodo");
            $titulaciones=\FCS\NivelEstudio::select("nombre_nivelestudio",\DB::raw("count(titulacion_docentes.id)"))
                            ->join("titulacion_docentes","titulacion_docentes.id_nivelestudio","=","nivel_estudios.id")
                            ->where("titulacion_docentes.id_periodo","=",\DB::raw($periodo))
                            ->groupby("nombre_nivelestudio")->get();
            foreach ($titulaciones as $t) {
                $datos1[]=$t->nombre_nivelestudio;
                $datos2[]=$t->count;
            }
            $tituloX="Nivel de Estudio";
            $tituloY="Total Profesores";
            $titulografica = "Nivel De Estudio Docente Por Periodo Academico";
            $nombreSerie="Cantidad de Docentes por Nivel de Estudio";
            return \Response::json(compact('datos1','datos2','tituloX','tituloY','nombreSerie','titulografica'));
        }elseif ($id==1) {
            $datos1 = [];
            $datos2= [];
            $periodo=\Input::get("periodo");
            $evaluaciones=\FCS\EvaluacionDocente::select("nota",\DB::raw("count(nota)"))
                            ->where("evaluacion_docentes.id_periodo","=",\DB::raw($periodo))
                            ->groupby("nota")
                            ->orderby("nota","asc")
                            ->get();
            foreach ($evaluaciones as $v) {
                $datos1[]=$v->nota;
                $datos2[]=$v->count;
            }
            $tituloX="Nota Cualitativa";
            $tituloY="Total Profesores";
            $titulografica = "Evaluacioón Docente Por Periodo Academico";
            $nombreSerie="Cantidad de Docentes por Nota De Evaluación";
            return \Response::json(compact('datos1','datos2','tituloX','tituloY','nombreSerie','titulografica'));
        }elseif ($id==2) {
            $datos1 = [];
            $datos2= [];
            $periodo=\Input::get("periodo");
            $responsabilidad= \DB::select('select count(id_vinculacion), nombre_vinculacion
                from responsabilidad_docentes, grupos, vinculaciones, periodos
                where responsabilidad_docentes.id_vinculacion = vinculaciones.id
                and responsabilidad_docentes.id_grupo = grupos.id
                and grupos.id_periodo = periodos.id
                and periodos.id = '.$periodo.' group by (nombre_vinculacion)');
            foreach ($responsabilidad as $r) {
                $datos1[]=$r->nombre_vinculacion;
                $datos2[]=$r->count;
            }
            $tituloX="Tipo Vinculación";
            $tituloY="Total Profesores";
            $titulografica = "Vinculación Docente Por Periodo Academico";
            $nombreSerie="Cantidad de Docentes por Vinculación";
            return \Response::json(compact('datos1','datos2','tituloX','tituloY','nombreSerie','titulografica'));
        }elseif ($id==3) {
            $datos1 = [];
            $datos2= [];
            $periodo=\Input::get("periodo");
            $movilidad= \DB::select('select count(id_evento), caracter_evento
                from movilidades,eventos, periodos
                where movilidades.id_evento = eventos.id
                and movilidades.id_periodo = periodos.id
                and periodos.id = '.$periodo.' group by (caracter_evento)');
            foreach ($movilidad as $m) {
                $datos1[]=$m->caracter_evento;
                $datos2[]=$m->count;
            }
            $tituloX="Caracter del Evento";
            $tituloY="Total Movilidades";
            $titulografica = "Movilidades Docentes Por Periodo Academico";
            $nombreSerie="Cantidad de Movilidades por Caracter del Evento";
            return \Response::json(compact('datos1','datos2','tituloX','tituloY','nombreSerie','titulografica'));
        }
        elseif ($id==4) {
            $datos1 = [];
            $datos2= [];
            $periodo=\Input::get("periodo");
            $tipoevento= \DB::select('select count(id_proyecto), tipo
                from responsabilidades_proyeccion,proyecto, periodos
                where responsabilidades_proyeccion.id_proyecto = proyecto.id
                and responsabilidades_proyeccion.id_periodo = periodos.id
                and periodos.id = '.$periodo.' group by (tipo)');
            foreach ($tipoevento as $m) {
                $datos1[]=$m->tipo;
                $datos2[]=$m->count;
            }
            $tituloX="Tipo de Proyecto";
            $tituloY="Total de Proyectos";
            $titulografica = "Proyectos Por Periodo Academico";
            $nombreSerie="Cantidad de Proyectos por Tipo de Proyectos";
            return \Response::json(compact('datos1','datos2','tituloX','tituloY','nombreSerie','titulografica'));
        }elseif ($id==5) {
            $datos1 = [];
            $datos2= [];
            $periodo=\Input::get("periodo");
            $datos2[]=ResponsabilidadDocente::where("grupos.id_periodo","=",$periodo)->join("grupos","grupos.id","=","id_grupo")->sum("total_horas")+0;
            $datos2[]=ResponsabilidadInvestigacion::where("id_periodo","=",$periodo)->sum("horas_semanal")+0;
            $datos2[]=ResponsabilidadProyeccion::where("id_periodo","=",$periodo)->sum("horas_semanal")+0;
            $datos2[]=ResponsabilidadAdministrativa::where("id_periodo","=",$periodo)->sum("horas_semanal")+0;
            $datos2[]=ResponsabilidadOtras::where("id_periodo","=",$periodo)->sum("horas_semanal")+0;
            $total=0;
            for($i=0;$i<5;$i++)
                $total+=$datos2[$i];
            for($i=0;$i<5;$i++)
                $datos2[$i]=$datos2[$i]*100/$total;
            $datos1[]="Academica";
            $datos1[]="Investigacion";
            $datos1[]="Proyeccion";
            $datos1[]="Adminsitrativa";
            $datos1[]="Otras";
            /*foreach ($tipoevento as $m) {
                $datos1[]=$m->tipo;
                $datos2[]=$m->count;
            }*/
            $tituloX="Tipo Responsabilidad";
            $tituloY="Porcentaje ";
            $titulografica = "Horas dedicadas a cada Responsabilidad";
            $nombreSerie="Porcentaje de horas por Responsabilidad";
            return \Response::json(compact('datos1','datos2','tituloX','tituloY','nombreSerie','titulografica'));
        }elseif ($id==6) {
            $datos1 = [];
            $datos2 = [];
            $periodo=\Input::get("periodo");
            $datos2[0]=ResponsabilidadDocente::where("grupos.id_periodo","=",$periodo)->join("grupos","grupos.id","=","id_grupo")->sum("total_horas")+0;
            $datos2[1]=ResponsabilidadInvestigacion::where("id_periodo","=",$periodo)->sum("horas_semanal")+0;
            $datos2[2]=ResponsabilidadProyeccion::where("id_periodo","=",$periodo)->sum("horas_semanal")+0;
            $datos2[3]=ResponsabilidadAdministrativa::where("id_periodo","=",$periodo)->sum("horas_semanal")+0;
            $datos2[4]=ResponsabilidadOtras::where("id_periodo","=",$periodo)->sum("horas_semanal")+0;
            $datos1[0]="Academica";
            $datos1[1]="Investigacion";
            $datos1[2]="Proyeccion";
            $datos1[3]="Adminsitrativa";
            $datos1[4]="Otras";
            /*foreach ($tipoevento as $m) {
                $datos1[]=$m->tipo;
                $datos2[]=$m->count;
            }*/
            $tituloX="Tipo Responsabilidad";
            $tituloY="# de horas ";
            $titulografica = "Horas dedicadas a cada Responsabilidad";
            $nombreSerie="# de horas por Responsabilidad";
            return \Response::json(compact('datos1','datos2','tituloX','tituloY','nombreSerie','titulografica'));
        }elseif ($id==7) {
            $datos1 = [];
            $datos2 = [];
            $periodo=\Input::get("periodo");
            $a=ResponsabilidadDocente::select("id_profesor")->where("grupos.id_periodo","=",$periodo)->join("grupos","grupos.id","=","id_grupo")->groupby("id_profesor")->get();
            $i=ResponsabilidadInvestigacion::select("id_profesor")->where("id_periodo","=",$periodo)->groupby("id_profesor")->get();
            $p=ResponsabilidadProyeccion::select("id_profesor")->where("id_periodo","=",$periodo)->groupby("id_profesor")->get();
            $ad=ResponsabilidadAdministrativa::select("id_profesor")->where("id_periodo","=",$periodo)->groupby("id_profesor")->get();
            $ot=ResponsabilidadOtras::select("id_profesor")->where("id_periodo","=",$periodo)->groupby("id_profesor")->get();
            $docentes=[];
            foreach($a as $r)
                if(!in_array($r->id_profesor,$docentes))
                    $docentes[]=$r->id_profesor;
            foreach($i as $r)
                if(!in_array($r->id_profesor,$docentes))
                    $docentes[]=$r->id_profesor;
            foreach($p as $r)
                if(!in_array($r->id_profesor,$docentes))
                    $docentes[]=$r->id_profesor;
            foreach($ad as $r)
                if(!in_array($r->id_profesor,$docentes))
                    $docentes[]=$r->id_profesor;
            foreach($ot as $r)
                if(!in_array($r->id_profesor,$docentes))
                    $docentes[]=$r->id_profesor;
            $datos2[0]=count($a)*100/count($docentes);
            $datos2[1]=count($i)*100/count($docentes);
            $datos2[2]=count($p)*100/count($docentes);
            $datos2[3]=count($ad)*100/count($docentes);
            $datos2[4]=count($ot)*100/count($docentes);
            $datos1[0]="Academica";
            $datos1[1]="Investigacion";
            $datos1[2]="Proyeccion";
            $datos1[3]="Adminsitrativa";
            $datos1[4]="Otras";
            /*foreach ($tipoevento as $m) {
                $datos1[]=$m->tipo;
                $datos2[]=$m->count;
            }*/
            $tituloX="Tipo Responsabilidad";
            $tituloY="% de Docentes";
            $titulografica = "Porcentaje de Dedicacion Docente por Responsabilidad";
            $nombreSerie="% de Docentes por Responsabilidad";
            return \Response::json(compact('datos1','datos2','tituloX','tituloY','nombreSerie','titulografica'));
        }
    }

}