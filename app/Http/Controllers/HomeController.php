<?php

namespace FCS\Http\Controllers;

use Illuminate\Http\Request;
use FCS\Http\Requests;
use FCS\Http\Controllers\Controller;

use DB, View, Session, Redirect;

class HomeController extends Controller
{
    
    public function index()
    {
        return view('prueba');
    }

    public function home()
    {
        return redirect("/");
    }

    public function carga()
    {
        return view('lapa/lapita');
    }

    public function grafica()
    {
        $periodos=\FCS\Periodo::orderby("anio","desc")->orderby("periodo","desc")->get()->lists("NombrePeriodo","id");
        return view ('grafica',compact('periodos'));
    }

    public function grafJason($id)
    {
        if($id==0){
            $datos1 = [];
            $datos2= [];
            $periodo=\Input::get("periodo");
            $titulaciones=\FCS\NivelEstudio::select("nombre_nivelestudio",\DB::raw("count(titulacion_docentes.id)"))
                            ->join("titulacion_docentes","titulacion_docentes.id_nivelestudio","=","nivel_estudios.id")
                            ->where("titulacion_docentes.id_periodo","=",\DB::raw($periodo))
                            ->groupby("nombre_nivelestudio")->get();
            foreach ($titulaciones as $t) {
                $datos1[]=$t->nombre_nivelestudio;
                $datos2[]=$t->count;
            }
            $tituloX="Nivel de Estudio";
            $tituloY="Total Profesores";
            $titulografica = "Nivel De Estudio Docente Por Periodo Academico";
            $nombreSerie="Cantidad de Docentes por Nivel de Estudio";
            return \Response::json(compact('datos1','datos2','tituloX','tituloY','nombreSerie','titulografica'));
        }elseif ($id==1) {
            $datos1 = [];
            $datos2= [];
            $periodo=\Input::get("periodo");
            $evaluaciones=\FCS\EvaluacionDocente::select("nota",\DB::raw("count(nota)"))
                            ->where("evaluacion_docentes.id_periodo","=",\DB::raw($periodo))
                            ->groupby("nota")
                            ->orderby("nota","asc")
                            ->get();
            foreach ($evaluaciones as $v) {
                $datos1[]=$v->nota;
                $datos2[]=$v->count;
            }
            $tituloX="Nota Cualitativa";
            $tituloY="Total Profesores";
            $titulografica = "Evaluacioón Docente Por Periodo Academico";
            $nombreSerie="Cantidad de Docentes por Nota De Evaluacioón";
            return \Response::json(compact('datos1','datos2','tituloX','tituloY','nombreSerie','titulografica'));
        }elseif ($id==2) {
            $datos1 = [];
            $datos2= [];
            $periodo=\Input::get("periodo");
            $responsabilidad= \DB::select('select count(id_vinculacion), nombre_vinculacion
                from responsabilidad_docentes, grupos, vinculaciones, periodos
                where responsabilidad_docentes.id_vinculacion = vinculaciones.id
                and responsabilidad_docentes.id_grupo = grupos.id
                and grupos.id_periodo = periodos.id
                and periodos.id = '.$periodo.' group by (nombre_vinculacion)');
            foreach ($responsabilidad as $r) {
                $datos1[]=$r->nombre_vinculacion;
                $datos2[]=$r->count;
            }
            $tituloX="Tipo Vinculación";
            $tituloY="Total Profesores";
            $titulografica = "Vinculación Docente Por Periodo Academico";
            $nombreSerie="Cantidad de Docentes por Vinculación";
            return \Response::json(compact('datos1','datos2','tituloX','tituloY','nombreSerie','titulografica'));
        }
    }

}
