<?php

namespace FCS\Http\Controllers;

use FCS\Actividad;

use Illuminate\Http\Request;
use FCS\Http\Requests;
use FCS\Http\Requests\Actividades\CreateRequest;
use FCS\Http\Controllers\Controller;

use DB, View, Session, Redirect;

class ActividadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!\FCS\Permiso::verificarPermiso("actividades"))
        return redirect("/");

        $actividad=Actividad::All();
        return view('componentes.actividades.index',compact('actividad'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if(!\FCS\Permiso::verificarPermiso("actividades"))
        return redirect("/");

        $route = [ 'route' => 'actividades.store' ];
        return view('componentes.actividades.add',compact('route'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(CreateRequest $request)
    {
        if(!\FCS\Permiso::verificarPermiso("actividades"))
        return redirect("/");

        Actividad::create($request->all());
        return redirect('actividades')->with('message','Actividad Creada Exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        if(!\FCS\Permiso::verificarPermiso("actividades"))
        return redirect("/");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if(!\FCS\Permiso::verificarPermiso("actividades"))
        return redirect("/");

        $actividad=Actividad::find($id);
        $route = [ 'route'=>['actividades.update',$actividad->id],'method'=>'PUT'];
        return view('componentes.actividades.edit', compact('route','actividad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(CreateRequest $request, $id)
    {
        if(!\FCS\Permiso::verificarPermiso("actividades"))
        return redirect("/");

        $actividad=Actividad::find($id);
        $actividad->fill($request->all());
        $actividad->save();

        Session::flash('message','Actividad Editada Correctamente');
        return redirect::to('actividades');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if(!\FCS\Permiso::verificarPermiso("actividades"))
        return redirect("/");
    
        Actividad::destroy($id);
        Session::flash('message','Actividad Eliminada Correctamente');
        return Redirect::to('actividades');
    }
}
