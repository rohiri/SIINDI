<?php

namespace FCS\Http\Controllers;

use FCS\FuncionAdministrativa;
use Illuminate\Http\Request;
use FCS\Http\Requests;
use FCS\Http\Controllers\Controller;
use FCS\Http\Requests\FunAdmin\CreateRequest;

use DB, View, Session, Redirect;

class FuncionAdministrativaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
        if(!\FCS\Permiso::verificarPermiso("funciones-administrativas"))
        return redirect("/");

        $funcion=FuncionAdministrativa::All();
        return view('componentes.funcion_administrativa.index',compact('funcion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if(!\FCS\Permiso::verificarPermiso("funciones-administrativas"))
        return redirect("/");

        $route = [ 'route' => 'funciones-administrativas.store' ];
        return view('componentes.funcion_administrativa.add',compact('route'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(CreateRequest $request)
    {
        if(!\FCS\Permiso::verificarPermiso("funciones-administrativas"))
        return redirect("/");

        FuncionAdministrativa::create($request->all());
        return redirect('funciones-administrativas')->with('message','Funcion Administrativa Creada Exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        if(!\FCS\Permiso::verificarPermiso("funciones-administrativas"))
        return redirect("/");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if(!\FCS\Permiso::verificarPermiso("funciones-administrativas"))
        return redirect("/");

        $funcion=FuncionAdministrativa::find($id);
        $route = [ 'route'=>['funciones-administrativas.update',$funcion->id],'method'=>'PUT'];
        return view('componentes.funcion_administrativa.edit', compact('route','funcion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(CreateRequest $request, $id)
    {
        if(!\FCS\Permiso::verificarPermiso("funciones-administrativas"))
        return redirect("/");

        $funcion=FuncionAdministrativa::find($id);
        $funcion->fill($request->all());
        $funcion->save();

        Session::flash('message','Función Administrativa Editada Correctamente');
        return redirect::to('funciones-administrativas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if(!\FCS\Permiso::verificarPermiso("funciones-administrativas"))
        return redirect("/");
    
        FuncionAdministrativa::destroy($id);
        Session::flash('message','Función Administrativa Eliminada Correctamente');
        return Redirect::to('funciones-administrativas');
    }
}
