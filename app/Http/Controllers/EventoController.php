<?php

namespace FCS\Http\Controllers;

use Illuminate\Http\Request;

use FCS\Evento;
use FCS\TipoEvento;

use FCS\Http\Requests;
use FCS\Http\Controllers\Controller;
use FCS\Http\Requests\Evento\CreateRequest;

use DB, View, Session, Redirect;

class EventoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if(!\FCS\Permiso::verificarPermiso("evento"))
        return redirect("/");

        $evento=Evento::All();
        return view('componentes.eventos.index',compact('evento'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if(!\FCS\Permiso::verificarPermiso("evento"))
        return redirect("/");

        $tipo_evento = TipoEvento::allLists();
        $route = [ 'route' => 'evento.store' ];
        return view('componentes.eventos.addevento',compact('tipo_evento','route'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(CreateRequest $request)
    {
        if(!\FCS\Permiso::verificarPermiso("evento"))
        return redirect("/");
        Evento::create($request->all());
        return redirect('evento')->with('message','Evento creado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        if(!\FCS\Permiso::verificarPermiso("evento"))
        return redirect("/");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if(!\FCS\Permiso::verificarPermiso("evento"))
        return redirect("/");

        $tipo_evento = TipoEvento::allLists();
        $eventos=Evento::find($id);
        $route = [ 'route'=>['evento.update',$eventos->id],'method'=>'PUT'];
        return view('componentes.eventos.editevento', compact('tipo_evento','route','eventos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(CreateRequest $request, $id)
    {
        if(!\FCS\Permiso::verificarPermiso("evento"))
        return redirect("/");

        $tipo_eventos=TipoEvento::allLists();
        $eventos=Evento::find($id);
        $eventos->fill($request->all());
        $eventos->save();

        Session::flash('message','Evento Editado Correctamente');
        return redirect::to('evento');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if(!\FCS\Permiso::verificarPermiso("evento"))
        return redirect("/");
    
        Evento::destroy($id);
        Session::flash('message','Evento Eliminado Correctamente');
        return Redirect::to('/evento');
    }
}