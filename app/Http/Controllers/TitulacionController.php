<?php

namespace FCS\Http\Controllers;

use Illuminate\Http\Request;
use FCS\Http\Requests;
use FCS\Http\Controllers\Controller;
use FCS\Http\Requests\Titulacion\CreateRequest;

use FCS\TitulacionDocente;
use FCS\Profesor;
use FCS\Periodo;
use FCS\Programa;
use FCS\NivelEstudio;

use DB, View, Session, Redirect;

class TitulacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!\FCS\Permiso::verificarPermiso("titulacion-docente"))
        return redirect("/");
        $titulacion=TitulacionDocente::All();
        return view('componentes.titulacion_docente.index',compact('titulacion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!\FCS\Permiso::verificarPermiso("titulacion-docente"))
        return redirect("/");
        $titulacion= new \FCS\TitulacionDocente;
        $programa = Programa::ListaPrograma();
        $profesor = Profesor::get()->lists("NombreProfesores","id");
        $periodo = Periodo::get()->lists("NombrePeriodo","id");
        $nivel = NivelEstudio::get()->lists("nombre_nivelestudio","id");
        $route = [ 'route' => 'titulacion-docente.store'];
        return view('componentes.titulacion_docente.addtitulaciondocente',compact('route','programa','profesor','periodo','nivel'));
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        if(!\FCS\Permiso::verificarPermiso("titulacion-docente"))
        return redirect("/");
        TitulacionDocente::create($request->all());
        return redirect('titulacion-docente')->with('message','Titulacion Docente creada exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!\FCS\Permiso::verificarPermiso("titulacion-docente"))
        return redirect("/");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!\FCS\Permiso::verificarPermiso("titulacion-docente"))
        return redirect("/");
        $titulacion= TitulacionDocente::find($id);
        $programa = Programa::ListaPrograma();
        $profesor = Profesor::get()->lists("NombreProfesores","id");
        $periodo = Periodo::get()->lists("NombrePeriodo","id");
        $nivel = NivelEstudio::get()->lists("nombre_nivelestudio","id");
        $route = [ 'route'=>['titulacion-docente.update',$titulacion->id],'method'=>'PUT'];
        return view('componentes.titulacion_docente.edittitulacion', compact('titulacion','route','profesor','periodo','nivel','programa'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateRequest $request, $id)
    {
        if(!\FCS\Permiso::verificarPermiso("titulacion-docente"))
        return redirect("/");
        $programa =Programa::ListaPrograma();
        $profesor = Profesor::get()->lists("NombreProfesores","id");
        $periodo = Periodo::get()->lists("NombrePeriodo","id");
        $nivel = NivelEstudio::get()->lists("nombre_nivelestudio","id");
        $titulacion=TitulacionDocente::find($id);
        $titulacion->fill($request->all());
        $titulacion->save();

        Session::flash('message','Titulacion Docente Editada Correctamente');
        return redirect::to('titulacion-docente');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!\FCS\Permiso::verificarPermiso("titulacion-docente"))
        return redirect("/");
        TitulacionDocente::destroy($id);
        Session::flash('message','Titulacion Docente Eliminada Correctamente');
        return Redirect::to('/titulacion-docente');
    }
}