<?php

namespace FCS\Http\Controllers;

use Illuminate\Http\Request;

use FCS\Http\Requests;
use FCS\Http\Controllers\Controller;

use FCS\Movilidad;
use FCS\Profesor;
use FCS\Periodo;
use FCS\Evento;
use FCS\TipoEvento;
use FCS\Http\Requests\Movilidades\CreateRequest;
use DB, View, Session, Redirect;

class MovilidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //if(!\FCS\Permiso::verificarPermiso("movilidad-docente"))
        //return redirect("/");
        $movilidades=\FCS\Movilidad::select(\DB::raw("id,id_profesor,id_periodo,id_evento"))->where("tipo_movilidad","=","Profesor")->orderby("id_periodo","desc")->get();
        return view('componentes.Movilidad.index',compact('movilidades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //if(!\FCS\Permiso::verificarPermiso("responsabilidad-docente"))
        //return redirect("/");
        $movilidades= new \FCS\Movilidad;
        $profesor = Profesor::get()->lists("NombreProfesores","id");
        $periodo = Periodo::get()->lists("NombrePeriodo","id"); 
        $evento = Evento::get()->lists("NombreEvento","id");
        $route = [ 'route' => 'movilidad-docente.store'];
        return view('componentes.Movilidad.add',compact('route','movilidades','profesor','periodo','evento'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $movilidades=new Movilidad;
        $movilidades->fill($request->all());
        $movilidades->save();
        return redirect('movilidad-docente')->with('message','Movilidad Docente Creada Exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $movilidades=Movilidad::find($id);
        $profesor = Profesor::get()->lists("NombreProfesores","id");
        $periodo = Periodo::get()->lists("NombrePeriodo","id"); 
        $evento = Evento::get()->lists("NombreEvento","id");
        $route = [ 'route'=>['movilidad-docente.update',$movilidades->id],'method'=>'PUT'];
        return view('componentes.Movilidad.edit', compact('movilidades','route','evento','profesor','periodo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateRequest $request, $id)
    {
        $movilidades=Movilidad::find($id);
        $profesor = Profesor::get()->lists("NombreProfesores","id");
        $periodo = Periodo::get()->lists("NombrePeriodo","id"); 
        $evento = Evento::get()->lists("NombreEvento","id");
        $movilidades->fill($request->all());
        $movilidades->save();

        Session::flash('message','Movilidad Editada Correctamente');
        return redirect::to('movilidad-docente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Movilidad::destroy($id);
        Session::flash('message','Movilidad Eliminada Correctamente');
        return Redirect::to('movilidad-docente');
    }
}
