<?php
namespace FCS\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use FCS\User;

use FCS\Http\Requests;
use Session;
use Redirect;
use FCS\Http\Controllers\Controller;
use FCS\Http\Requests\usuario\userCreate;
use FCS\Http\Requests\usuario\userUpdate;


class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $usuarios=User::All();
        return view('usuario.index',compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $usuario=new \FCS\User;
        $menus=\FCS\Menu::orderby("nombre","asc")->get();
        $route = array('url' => 'usuario', 'method'=>'POST','autocomplete'=>'off');
        return view('usuario.create',compact('estudiantes','estudiante','menus','route','usuario'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(userCreate $request)
    {
        $data=\Input::all();    
        \DB::transaction(function()use($data){
            $user=new \FCS\User;
            $data["password"]=\Hash::make($data["password"]);
            $user->fill($data);
            if(!array_key_exists("estudiantechk", $data))
                $data["estudiante"]=0;
            $user->estudiante=$data["estudiante"];
            $user->save();
            $user=\FCS\User::orderby("updated_at","desc")->first();
            if(array_key_exists("permiso", $data))
                foreach ($data["permiso"] as $id => $valor) {
                    $permiso=new \FCS\Permiso;
                    $permiso->iduser=$user->id;
                    $permiso->idmenus=$id;
                    $permiso->permiso=1;
                    $permiso->save();
                }
        });
        return redirect('usuario')->with('message','Usuario creado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $usuario=\FCS\User::find($id);
        $estudiantes=\FCS\Estudiante::get()->lists("NombreConCodigo","id");
        if($usuario->estudiante>0)
            $estudiante="checked";
        else
            $estudiante="";
        $menus=\FCS\Menu::select("menus.id","permiso","menus.nombre")->leftjoin("permisos",function($join)use($id){
            $join->on("idmenus","=","menus.id")
            ->on("iduser","=",\DB::raw($id));
        })->orderby("nombre","asc")->get();
        $route = array('url' => 'usuario/'.$id, 'method'=>'PATCH');
        return view('usuario.edit',compact('estudiantes','estudiante','menus','route','usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(userUpdate $request, $id)
    {
        $data=\Input::all();    
        \DB::transaction(function()use($data,$id){
            $permisos=\FCS\Permiso::where("iduser","=",\DB::raw($id))->get();
            foreach ($permisos as $p) {
                $p->delete();
            }
            $user=\FCS\User::find($id);
            if($data["password"]!="")
                $data["password"]=\Hash::make($data["password"]);
            else
                unset($data["password"]);
            $user->fill($data);
            if(!array_key_exists("estudiantechk", $data))
                $data["estudiante"]=0;
            $user->estudiante=$data["estudiante"];
            $user->save();
            $user=\FCS\User::orderby("updated_at","desc")->first();
            if(array_key_exists("permiso", $data))
                foreach ($data["permiso"] as $id => $valor) {
                    $permiso=new \FCS\Permiso;
                    $permiso->iduser=$user->id;
                    $permiso->idmenus=$id;
                    $permiso->permiso=1;
                    $permiso->save();
                }
        });
        return redirect('usuario')->with('message','Usuario editado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
       \DB::transaction(function()use($id){
            $permisos=\FCS\Permiso::where("iduser","=",\DB::raw($id))->get();
            foreach ($permisos as $p) {
                $p->delete();
            }
            $user=\FCS\User::find($id);
            $user->delete();
        });
        return redirect('usuario')->with('message','Usuario eliminado exitosamente');
    }
}