<?php

namespace FCS\Http\Controllers;

use Illuminate\Http\Request;
use FCS\TipoEvento;
use FCS\Http\Requests;
use Session;
use Redirect;
use FCS\Http\Requests\TipoEvento\CreateRequest;
use FCS\Http\Controllers\Controller;

class TipoEventoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if(!\FCS\Permiso::verificarPermiso("tipo-evento"))
        return redirect("/");

        $tipo_eventos=TipoEvento::All();
        return view('componentes.tipo_evento.index',compact('tipo_eventos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if(!\FCS\Permiso::verificarPermiso("tipo-evento"))
        return redirect("/");
        return view('componentes.tipo_evento.addtipoevento');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(CreateRequest $request)
    {
        if(!\FCS\Permiso::verificarPermiso("tipo-evento"))
        return redirect("/");

        TipoEvento::create($request->all());
        return redirect('tipo-evento')->with('message','Tipo Evento creado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        if(!\FCS\Permiso::verificarPermiso("tipo-evento"))
        return redirect("/");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if(!\FCS\Permiso::verificarPermiso("tipo-evento"))
        return redirect("/");

        $tipo_eventos=TipoEvento::find($id);
        $route = [ 'route'=>['tipo-evento.update',$tipo_eventos->id],'method'=>'PUT'];
        return view('componentes.tipo_evento.edittipoevento',compact('route','tipo_eventos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(CreateRequest $request, $id)
    {
        if(!\FCS\Permiso::verificarPermiso("tipo-evento"))
        return redirect("/");

        $tipo_eventos=TipoEvento::find($id);
        $tipo_eventos->fill($request->all());
        $tipo_eventos->save();

        Session::flash('message','Tipo Evento Editado Correctamente');
        return redirect::to('tipo-evento');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if(!\FCS\Permiso::verificarPermiso("tipo-evento"))
        return redirect("/");
    
        TipoEvento::destroy($id);
        Session::flash('message','Tipo Evento Eliminado Correctamente');
        return Redirect::to('/tipo-evento');
    }
}
