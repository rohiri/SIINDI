<?php

namespace FCS\Http\Controllers;

use Illuminate\Http\Request;
use FCS\User;
use FCS\Http\Requests;
use Session;
use Redirect;
use FCS\Http\Controllers\Controller;
use Auth;
use Input;

class grupoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $grupos=\FCS\Grupo::select(\DB::raw("grupos.id,id_curso,id_periodo,nombre_curso,codigo,anio,periodo"))->join("cursos","cursos.id","=","grupos.id_curso")->join("periodos","periodos.id","=","grupos.id_periodo")->orderby("periodo","desc")->get();
        return view("formulario programa.Secretaria.Grupo.index",compact("grupos"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $grupo= new \FCS\Grupo;
        $cursos=\FCS\Curso::get()->lists("nombre_curso","id");
        $periodos=\FCS\Periodo::get()->lists("NombrePeriodo","id");
        $form_data=array("url"=>"grupo","method"=>"POST");
        return view("formulario programa.Secretaria.Grupo.Agregar_Grupo",compact("form_data","grupo","cursos","periodos"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store()
    {
        $data=Input::all();
        \DB::transaction(function()use($data){
            $grupo=new \FCS\Grupo;
            $grupo->fill($data);
            $grupo->save();
        });
        return redirect("grupo")->with("mensaje","El grupo fue guardado!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $grupo=\FCS\Grupo::find($id);
        $cursos=\FCS\Curso::get()->lists("nombre_curso","id");
        $periodos=\FCS\Periodo::get()->lists("NombrePeriodo","id");
        $form_data=array("route"=>array("grupo.update",$id),"method"=>"PATCH");
        return view("formulario programa.Secretaria.Grupo.Agregar_Grupo",compact("form_data","grupo","cursos","periodos"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data=Input::all();
        \DB::transaction(function()use($data,$id){
            $grupo=\FCS\Grupo::find($id);
            $grupo->fill($data);
            $grupo->save();
        });
        return redirect("grupo")->with("mensaje","El grupo fue actualizado!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        \DB::transaction(function()use($id){
            $grupo=\FCS\Grupo::find($id);
            $grupo->delete();
        });
        return redirect("grupo")->with("mensaje-error","El grupo fue eliminado!");
    }
}
