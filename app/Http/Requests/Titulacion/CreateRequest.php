<?php 
namespace FCS\Http\Requests\Titulacion;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest {

    public function rules()
    {
        return [

                'id_profesor'=>'required',
                'id_programa'=>'required',
                'id_nivelestudio'=>'required',
                'titulo_estudio'=>'required|min:10|max:200',
                'id_periodo'=>'required',
        ];
    }

    public function authorize()
    {
        return true;
    }

}
