<?php 
namespace FCS\Http\Requests\Movilidades;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest {

    public function rules()
    {
        return [
                'tipo_movilidad'=>'required',
                'id_profesor'   =>'required',
                'id_periodo'    =>'required',
                'id_evento'     =>'required',
                
                
        ];
    }

    public function authorize()
    {
        return true;
    }

}
