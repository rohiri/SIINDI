<?php 
namespace FCS\Http\Requests\NivelEstudio;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest {

    public function rules()
    {
        return [

                'nombre_nivelestudio' => 'required|min:5|max:70',
        ];
    }

    public function authorize()
    {
        return true;
    }

}
