<?php
namespace FCS\Http\Requests\usuario;

use Illuminate\Foundation\Http\FormRequest;

class userCreate extends FormRequest {

    public function rules()
    {
        return ['name'  => 'required|min:3|max:28',
                'email' => 'required|email|unique:users',
        ];
    }

    public function authorize()
    {
        return true;
    }
}