<?php
namespace FCS\Http\Requests\usuario;

use Illuminate\Foundation\Http\FormRequest;

class userUpdate extends FormRequest {

    public function rules()
    {
        return ['name'  => 'required|min:3|max:28',       
        ];
    }

    public function authorize()
    {
        return true;
    }
}