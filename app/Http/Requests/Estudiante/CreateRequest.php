<?php

namespace FCS\Http\Requests\Estudiante;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest {

    public function rules()
    {
        return [
                'id_periodo'                    =>'required|unique:estudiantes_preu,id_periodo',
                'e_inscritos'                   =>'required|regex:/^\d{1,4}$/',
                'e_admintidos'                  =>'required|regex:/^\d{2,4}$/',
                'matriculados_total'            =>'required|regex:/^\d{2,4}$/',
                'matriculados_primersemestre'   =>'required|regex:/^\d{2,4}$/',
                'graduados'                     =>'required|regex:/^\d{1,4}$/',
                'retirados'                     =>'required|regex:/^\d{1,4}$/',
                'tasa_desercion'                =>'required|regex:/^\d{1,2}$/',
                'tasa_culminacion'              =>'required|regex:/^\d{1,2}$/',
                
                ];
    }
    
    public function authorize()
    {
        return true;
    }
}