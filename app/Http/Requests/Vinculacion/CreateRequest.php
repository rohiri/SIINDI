<?php 
namespace FCS\Http\Requests\Vinculacion;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest {

    public function rules()
    {
        return [

                'nombre_vinculacion' => 'required|min:4|max:90',
        ];
    }

    public function authorize()
    {
        return true;
    }

}
