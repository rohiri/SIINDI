<?php 
namespace FCS\Http\Requests\RespDocen;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest {

	public function rules()
    {
        return [

                'id_profesor'           =>'required',
                'id_grupo'              =>'required',
                'id_vinculacion'        =>'required',
                'numero_estudiantes'    =>'required|regex:/^\d{1,2}$/',
                'horas_directas'        =>'required|regex:/^\d{1,2}$/',
                'horas_tutoria'         =>'required|regex:/^\d{1,2}$/',
                'horas_preparacion'     =>'required|regex:/^\d{1,2}$/',
                'numero_semanas'        =>'required|regex:/^\d{1,2}$/',
                'horas_semanal'     =>'regex:/^\d{1,2}$/',
                'id_periodo'            =>'required',
                'id_actividad'          =>'required',
                'fecha_inicio'          =>'required',
                'fecha_terminacion'     =>'required',
                'fecha_entregainforme'  =>'required',
                'id_funcion'        =>'required',
                ];
    }
    
    public function authorize()
    {
        return true;
    }
}