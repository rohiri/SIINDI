<?php 
namespace FCS\Http\Requests\FunAdmin;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest {

    public function rules()
    {
        return [
                'max_horas'         =>'required|regex:/^\d{1,2}$/',
                'nombre_funcion'    =>'required|min:4|max:250',
                'sesion_consejo'    =>'required|min:4|max:100',
                'fecha_sesion'      =>'required|date|',              
        ];
    }

    public function authorize()
    {
        return true;
    }
}