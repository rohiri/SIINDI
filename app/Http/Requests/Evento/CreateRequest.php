<?php 
namespace FCS\Http\Requests\Evento;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest {

    public function rules()
    {
        return [
                'numero_consejo' =>'required|min:4|max:100',
                'fecha'=>'required',
                'nombre_evento'=>'required|min:5|max:150',
                'lugar'=>'min:4|max:100',
                'caracter_evento'=>'required',
                'id_tipoeventos'=>'required', 
        ];
    }

    public function authorize()
    {
        return true;
    }
}
