<?php 
namespace FCS\Http\Requests\TipoEvento;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest {

    public function rules()
    {
        return [

                'nombre_tipoevento' => 'required|min:4|max:90',
        ];
    }

    public function authorize()
    {
        return true;
    }

}
