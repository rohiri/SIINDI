<?php 
namespace FCS\Http\Requests\EvaDocente;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest {

    public function rules()
    {
        return [
                'id_profesor'   =>'required',
                'id_periodo'    =>'required',
                'valor'         =>'required|regex:/^\d{1,2}$/',
                
                
        ];
    }

    public function authorize()
    {
        return true;
    }

}