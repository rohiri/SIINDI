<?php 
namespace FCS\Http\Requests\Actividades;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest {

    public function rules()
    {
        return [

                'nombre_actividad'=>'required|min:7|max:200',
                'sesion_consejo'=>'required|min:5|max:80',
                'fecha_sesion'=>'required',
               
        ];
    }

    public function authorize()
    {
        return true;
    }

}
