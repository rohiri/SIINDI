<?php 
namespace FCS\Http\Requests\Plan;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest {

	public function rules()
    {
        return [
                'codigo'            =>'required|unique:planes_estudios,codigo|regex:/^\d{6}$/',
                'fecha_resolucion'  =>'required',
                'numero_resolucion' =>'required|min:5|max:100',
                'fecha_inicio'      =>'required',
                'fecha_fin'         =>'required',
                'id_programa'       =>'required'
                ];
    }
    
    public function authorize()
    {
        return true;
    }
}