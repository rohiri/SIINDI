<?php 
namespace FCS\Http\Requests\Plan;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest {

	public function rules()
    {
        return [

        
                 'fecha_resolucion'  =>'required',
                'numero_resolucion' =>'required|min:5|max:100',
                'fecha_inicio'      =>'required',
                'fecha_fin'         =>'required',
                'id_programa'       =>'required'

                
                ];
    }
    
    public function authorize()
    {
        return true;
    }

}
