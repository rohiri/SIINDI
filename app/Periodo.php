<?php

namespace FCS;


use Illuminate\Database\Eloquent\Model;

class Periodo extends Model
{
    protected $table = "periodos";
    protected $primaryKey="id";
    protected $fillable = array("anio","periodo");

    public function getNombrePeriodoAttribute(){
    	return $this->attributes["anio"]." ".$this->attributes["periodo"];
    }

}