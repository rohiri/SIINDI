<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
       <strong>Desarrollado por:</strong> <strong><a href="https://www.facebook.com/Ricardo.WilliamTorres" TARGET="_new">William Torres</a></strong> y <strong><a href="#" TARGET="_new">Yusleidy Baquero</a></strong>
    </div>
    <!-- Default to the left -->
    <strong>Copyright © 2015 <a href="http://fcbi.unillanos.edu.co/proyectos/Facultad/php/index.php" TARGET="_new">Facultad Ciencias Basicas e Ingenieria</a>.</strong>
</footer>