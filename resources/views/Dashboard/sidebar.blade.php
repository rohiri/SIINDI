<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Panel Administración</li>

            <?php
                    $menus=\FCS\Permiso::join("menus","menus.id","=","idmenus")->where("iduser","=",DB::raw(Auth::user()->id))->where("permiso","=",DB::raw(1))->orderby("nombre","asc")->get();
                ?>
                @foreach($menus as $m)
                    <li>
                        <a href="{!! URL($m->ruta) !!}"><i class="{!! $m->imagen !!}"></i><span>{!! $m->nombre !!}</span></a>
                    </li>
                @endforeach

                <li class="header">Vinculos de Interes</li>
                  </ul><br><center>
                    <a href="http://sisepreu.unillanos.edu.co/MAUTE/login.php" target="_blank" class="btn btn-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PREU&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                    </center>
        </section>
        <!-- /.sidebar -->
    </section>
    <!-- /.sidebar -->
</aside>