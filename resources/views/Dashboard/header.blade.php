
<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a class="logo"><b>SIINDI</b>
    <img src="http://repositorio.unillanos.edu.co/retrieve/7"/>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="http://www.zayedhotel.com/addons/default/themes/yoona/img/user.jpg" class="user-image" alt="User Image" />
                  <span class="hidden-xs">{{Auth::user()->name }}</span>
                </a>
                <!-- User Account Menu -->
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="http://www.zayedhotel.com/addons/default/themes/yoona/img/user.jpg  " class="img-circle" alt="User Image" />
                    <p>
                      {{ Auth::user()->email}}<br>
                      <strong>Dependencia: </strong>{{ Auth::user()->dependencia}} 
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                    <p>
                      Usuario Desde
                      <br>{{ Auth::user()->created_at }}
                    </p>                   
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-right">
                      <a href="{{ URL::to('auth/logout') }}" class="btn btn-primary btn-sm">Cerrar Sesion</a>
                    </div>
                  </li>
                </ul>
              </li>      
            </ul>
        </div>
    </nav>
</header>