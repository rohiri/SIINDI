<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{{ $page_title or " SIINDI (Sistema de Información de Indicadores)" }}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.4 -->
        {!! Html::style('assets/bootstrap/css/bootstrap.min.css')!!}
        {!! Html::style('assets/plugins/iCheck/all.css')!!}
        <!-- Font Awesome Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        {!! Html::style('assets/plugins/select2/select2.min.css')!!}
        {!!Html::style('assets/plugins/sweetalert-master/dist/sweetalert.css')!!}
        <!-- DATA TABLES -->
        {!!Html::style('assets/plugins/datatables/dataTables.bootstrap.css')!!}
        <!-- Theme style -->
        {!!Html::style('assets/dist/css/AdminLTE.min.css')!!}
        {!!Html::style('assets/dist/css/skins/_all-skins.min.css')!!}
    </head>
    <body class="skin-red sidebar-mini">
        <div class="wrapper">
            <!-- Header -->
            @include('Dashboard.header')
            <!-- Sidebar -->
            @include('Dashboard.sidebar')
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
       
                <!-- Your Page Content Here -->
                @yield('content')
       
            </div><!-- /.content-wrapper -->

            <!-- Footer -->
            @include('Dashboard.footer')
        </div><!-- ./wrapper -->
        <!-- jQuery 2.1.4 -->
        {!!Html::script('assets/plugins/jQuery/jQuery-2.1.4.min.js')!!}
        {!!Html::script('assets/plugins/select2/select2.full.min.js')!!}
        <!-- Bootstrap 3.3.2 JS -->
        {!!Html::script('assets/bootstrap/js/bootstrap.min.js')!!}
        {!!Html::script('assets/plugins/sweetalert-master/dist/sweetalert.min.js')!!}
        <!-- DATA TABES SCRIPT-->
        {!!Html::script('assets/plugins/datatables/jquery.dataTables.min.js')!!}
        {!!Html::script('assets/plugins/datatables/dataTables.bootstrap.min.js')!!}  
        <!-- SlimScroll -->
        {!!Html::script('assets/plugins/slimScroll/jquery.slimscroll.min.js')!!}
        <!-- iCheck 1.0.1 -->
        {!!Html::script('assets/plugins/iCheck/icheck.min.js')!!}
        <!-- FastClick -->
        {!!Html::script('assets/plugins/fastclick/fastclick.min.js')!!}
        <!-- AdminLTE App -->
        {!!Html::script('assets/dist/js/app.min.js')!!}
        <!-- AdminLTE for demo purposes -->
        {!!Html::script('assets/dist/js/demo.js')!!}

        <!-- page script -->
        <script type="text/javascript">
            $(function () {
                $('#example3').DataTable({
                    language: {
                        lengthMenu:        "Mostrar _MENU_ Registros por Pagina",
                        search:            "Buscar ",
                        searchPlaceholder: "Busca por columnas",
                        info:              "Mostrar _START_ A _END_ De _TOTAL_ Registros",
                        zeroRecords:       "Ningun Registro Encontrado",
                        infoEmpty:         "No Hay Registros Disponibles",
                    paginate: {
                        first:         "Primero",
                        previous:      "<< Anterior",
                        next:          "Siguiente >>",
                        last:          "Ultimo"
                        }
                    }
                });
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });
                $(".select2").select2({
                    "language": {
                        "noResults": function(){
                            return "No Se Encontraron Resultados";
                        }
                    },
                    placeholder: "Seleccione Una Opcion"
                });
            });
        </script>
        @yield('js-adicional')
    </body>
</html>