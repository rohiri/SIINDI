<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{{ $page_title or "Sistema de Gestion de Indicadores CNA" }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap 3.3.4 -->
    {!! Html::style('assets2/css/bootstrap.min.css')!!}
     <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- DATA TABLES -->
    <!-- Theme style -->
    {!!Html::style('assets/dist/css/AdminLTE.css')!!}
    {!!Html::style('assets/dist/css/skins/_all-skins.min.css')!!}
    {!!Html::style('assets/plugins/iCheck/all.css')!!}
    {!!Html::style('assets2/css/style.css')!!}
    {!!Html::style('assets2/css/retina.min.css')!!}
    {!!Html::style('assets/plugins/datatables/dataTables.bootstrap.css')!!}
    {!!Html::style('assets/plugins/editor/css/editor.dataTables.min.css')!!}
</head>
<body class="skin-red sidebar-mini">
<div class="fixed wrapper">

    <!-- Header -->
    @include('Dashboard.header')

    <!-- Sidebar -->
    @include('Dashboard.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
       
                <!-- Your Page Content Here -->
                @yield('content')
       
    </div><!-- /.content-wrapper -->

    <!-- Footer -->
    @include('Dashboard.footer')
        </section><!-- /.content -->
    </div><!-- ./wrapper -->
    <!-- jQuery 2.1.4 -->
    {!!Html::script('assets2/js/jquery-2.1.0.min.js')!!}
    {!!Html::script('assets2/js/jquery-migrate-1.2.1.min.js')!!}
    {!!Html::script('assets2/js/bootstrap.min.js')!!}
        
    
    
    <!-- page scripts -->
    {!!Html::script('assets2/js/jquery-ui-1.10.3.custom.min.js')!!}
    {!!Html::script('assets2/js/jquery.ui.touch-punch.min.js')!!}
    {!!Html::script('assets2/js/jquery.sparkline.min.js')!!}
    {!!Html::script('assets2/js/fullcalendar.min.js')!!}

    {!!Html::script('assets2/js/jquery.flot.min.js')!!}
    {!!Html::script('assets2/js/jquery.flot.pie.min.js')!!}
    {!!Html::script('assets2/js/jquery.flot.stack.min.js')!!}
    {!!Html::script('assets2/js/jquery.flot.resize.min.js')!!}
    {!!Html::script('assets2/js/jquery.flot.time.min.js')!!}
    {!!Html::script('assets2/js/jquery.autosize.min.js')!!}
    {!!Html::script('assets2/js/jquery.placeholder.min.js')!!}
    
    <!-- theme scripts -->
    {!!Html::script('assets/plugins/iCheck/icheck.min.js')!!}
    {!!Html::script('assets2/js/custom.min.js')!!}
    {!!Html::script('assets2/js/core.min.js')!!}


    {!!Html::script('assets2/js/jquery-migrate-1.2.1.min.js')!!}
    {!!Html::script('assets2/js/bootstrap.min.js')!!}
    
        
    
    
    <!-- page scripts -->
    {!!Html::script('assets2/js/jquery-ui-1.10.3.custom.min.js')!!}
    {!!Html::script('assets2/js/jquery.sparkline.min.js')!!}
    {!!Html::script('assets2/js/jquery.chosen.min.js')!!}
    {!!Html::script('assets2/js/jquery.autosize.min.js')!!}
    {!!Html::script('assets2/js/jquery.placeholder.min.js')!!}
    {!!Html::script('assets2/js/wizard.min.js')!!}
    
    <!-- theme scripts -->
    {!!Html::script('assets2/js/custom.min.js')!!}
    {!!Html::script('assets2/js/core.min.js')!!}
    
    <!-- inline scripts related to this page -->
    {!!Html::script('assets2/js/pages/form-wizard.js')!!}


    
    <!-- inline scripts related to this page -->
    {!!Html::script('assets2/js/pages/index.js')!!}
    {!!Html::script('assets/plugins/datatables/jquery.dataTables.min.js')!!}
    {!!Html::script('assets/plugins/datatables/dataTables.bootstrap.min.js')!!}
    {!!Html::script('assets/plugins/mindmup-editabletable.js')!!}
    </script>
    @yield("js-adicional")
</body>
</html>