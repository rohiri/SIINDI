<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{{ $page_title or " SIINDI (Sistema de Información de Indicadores)" }}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.4 -->
        {!! Html::style('assets/bootstrap/css/bootstrap.min.css')!!}
        {!! Html::style('assets/plugins/iCheck/all.css')!!}
        <!-- Font Awesome Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        {!! Html::style('assets/plugins/select2/select2.min.css')!!}
        {!!Html::style('assets/plugins/sweetalert-master/dist/sweetalert.css')!!}
        <!-- DATA TABLES -->
        {!!Html::style('assets/plugins/datatables/dataTables.bootstrap.css')!!}
        <!-- Theme style -->
        {!!Html::style('assets/dist/css/AdminLTE.min.css')!!}
        {!!Html::style('assets/dist/css/skins/_all-skins.min.css')!!}
    </head>
    <body class="skin-red sidebar-mini">
        <div class="wrapper">
            <!-- Header -->
            @include('Dashboard.header')
            <!-- Sidebar -->
            @include('Dashboard.sidebar')
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
       
                <!-- Your Page Content Here -->
                @yield('content')
       
            </div><!-- /.content-wrapper -->

            <!-- Footer -->
            @include('Dashboard.footer')
        </div><!-- ./wrapper -->
        <!-- jQuery 2.1.4 -->
        {!!Html::script('assets/plugins/jQuery/jQuery-2.1.4.min.js')!!}
        {!!Html::script('assets/plugins/select2/select2.full.min.js')!!}
        <!-- Bootstrap 3.3.2 JS -->
        {!!Html::script('assets/bootstrap/js/bootstrap.min.js')!!}
        {!!Html::script('assets/plugins/sweetalert-master/dist/sweetalert.min.js')!!}
        <!-- DATA TABES SCRIPT-->
        {!!Html::script('assets/plugins/datatables/jquery.dataTables.min.js')!!}
        {!!Html::script('assets/plugins/datatables/dataTables.bootstrap.min.js')!!}  
        <!-- SlimScroll -->
        {!!Html::script('assets/plugins/slimScroll/jquery.slimscroll.min.js')!!}
        <!-- iCheck 1.0.1 -->
        {!!Html::script('assets/plugins/iCheck/icheck.min.js')!!}
        <!-- FastClick -->
        {!!Html::script('assets/plugins/fastclick/fastclick.min.js')!!}
        <!-- AdminLTE App -->
        {!!Html::script('assets/dist/js/app.min.js')!!}
        <!-- AdminLTE for demo purposes -->
        {!!Html::script('assets/dist/js/demo.js')!!}
        @yield('js-adicional')
    </body>
</html>