<form class="form-horizontal">
                <div class="row">
                  <div class="col-md-6">
                    {!! Form::label('Nombre de Usuario')!!}
                    {!!Form::text('name',$usuario->name,['class'=>'controls','class'=>'form-control focused sololetras','for'=>'focusedInput'])!!}
                  </div>
                </div>                
                <div class="row">
                  <div class="col-md-6">
                    {!!Form::label('Seleccionar Dependencia')!!}
                    {!!Form::select('dependencia',["Programa"=>"Programa","Secretaria"=>"Secretaria"],$usuario->dependencia,['class'=>'form-control select2','placeholder'=>'','required'])!!}
                  </div>
                  <div class="col-md-6">
                    {!!Form::label('Contraseña (Dejar en blanco para no cambiar)')!!}
                    {!!Form::password('password',['class'=>'form-control','autocomplete'=>'off'])!!}
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    Permisos
                    <div class="table-responsive">
                      <table class="table table-responsive">
                        <thead>
                          
                          <tr>
                            <th>Menu</th>
                            <th>¿Tiene Permiso?</th>
                          </tr>                          
                        </thead>
                        <tbody>
                          @foreach($menus as $m)
                            <tr>
                              <td>{!! $m->nombre !!}</td>
                              <td><input type="checkbox" name="permiso[{!! $m->id !!}]" class="flat-red" @if($m->permiso==1) checked="checked" @endif></td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div><br>         
                <div class="row form-group">
                  <div class="col-md-12"> 
                    {!!Form::submit('Guardar',['class'=>'btn btn-success'])!!}
                  </div>
                </div>
              </form>