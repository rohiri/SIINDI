<div class="form-group">
    {!!Form::label('Nombre De La Actividad')!!}
    {!!Form::text('nombre_actividad',null,['class'=>'form-control','placeholder'=>'Ingrese el Nombre de la Función Administrativa','required'])!!}

    {!!Form::label('Sesion del Consejo')!!}
    {!!Form::text('sesion_consejo',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del evento','required'])!!}

    {!!Form::label('Fecha de la Sesion')!!}<br>
    <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
        {!!Form::input('date', 'fecha_sesion', null, ['class' => 'form-control','required'])!!}
    </div>
</div>