@extends('layaouts.tablas')
@section('content')
  <section class="content"> 
    @include('componentes.actividades.partials.modal') 
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            @include('layaouts.partials.mensaje')
            <h3 class="box-title">Listado de Funciones Actividades</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="row form-group">
              <div class="col-md-3">
                <a href="{!! URL('actividades/create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Nueva Actividad</a>
              </div>
            </div>
            <table id="example3" class="table table-bordered table-striped">
              <thead>
                <th>Nombre Actividad</th>
                <th>Sesion Consejo</th>
                <th>Fecha Sesión</th>
                <th>Acciones</th>
              </thead>
              <tbody>
                @foreach($actividad as $a)
                  <tr>
                    <td>{{ $a->nombre_actividad}}</td>
                    <td>{{ $a->sesion_consejo}}</td>
                    <td>{{ $a->fecha_sesion}}</td>
                    <td>
                      {!! link_to_route('actividades.edit', $title='Editar', $parameters=$a->id, $atrributes=['class'=>'btn btn-warning']) !!}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>   
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div>                  
  </section><!-- /.content -->
@endsection