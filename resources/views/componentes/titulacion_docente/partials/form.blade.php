<div class="form-group">                           
    {!!Form::label('Profesor')!!}
    {!!Form::select('id_profesor', $profesor, null, ['class'=>'form-control select2','placeholder' => 'Seleccion el Profesor','required'])!!} <br>

    {!!Form::label('Nivel de Estudio')!!}
    {!!Form::select('id_nivelestudio', $nivel, null, ['class'=>'form-control select2','placeholder' => 'Seleccion el Nivel de Estudio','required'])!!} <br>
  
    {!!Form::label('Titulo de Estudio')!!}
    {!!Form::Text('titulo_estudio',null,['class'=>'form-control','placeholder'=>'Ingrese el Titulo de Estudio','required'])!!}
  
    {!!Form::label('Periodo')!!}<br>
    {!!Form::select('id_periodo', $periodo, null, ['class'=>'form-control select2','placeholder' => 'Seleccion el Periodo','required'])!!} <br>
  
    {!!Form::label('Programa')!!}<br>
    {!!Form::select('id_programa', $programa, null, ['class'=>'form-control select2','placeholder' => 'Seleccion el Programa','required'])!!} <br>
</div>