@extends('layaouts.tablas')
@section('content')
  <section class="content"> 
    @include('componentes.titulacion_docente.partials.modal')  
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            @include('layaouts.partials.mensaje')
            <h3 class="box-title">Listado de Titulación Docente</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="row form-group">
              <div class="col-md-3">
                <a href="{!! URL('titulacion-docente/create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Nueva Titualacion</a>
              </div>
            </div>
            <table id="example3" class="table table-bordered table-striped">
              <thead>
                <th>Profesor</th>
                <th>Programa</th>
                <th>Nivel Estudio</th>
                <th>Titulo</th>
                <th>Año</th>
                <th>Periodo</th>
                <th>Acciones</th>
              </thead>
              <tbody>
                @foreach($titulacion as $t)
                  <tr> 
                    <td>{{ $t->getProfesores->primer_nombre." ".$t->getProfesores->primer_apellido}}</td>
                    <td>{{ $t->getPrograma->nombre_programa}}</td>
                    <td>{{ $t->getNivel->nombre_nivelestudio}}</td>   
                    <td>{{ $t->titulo_estudio}}</td>
                    <td>{{ $t->getPeriodo->anio}}</td>
                    <td>{{ $t->getPeriodo->periodo}}</td>
                    <td>
                      {!! link_to_route('titulacion-docente.edit', $title='Editar', $parameters=$t->id, $atrributes=['class'=>'btn btn-warning']) !!}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>   
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div><!-- /.row -->                
  </section><!-- /.content -->
@endsection