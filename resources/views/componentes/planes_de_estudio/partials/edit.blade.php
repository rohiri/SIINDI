<div class="form-group">  
  

    {!!Form::label('Fecha de Resolución')!!}<br>
    <div class="input-group">
      <div class="input-group-addon">
        <i class="fa fa-calendar"></i>
      </div>
      {!!Form::input('date', 'fecha_resolucion', null, ['class' => 'form-control','required']);!!}
    </div><!-- /.input group -->
                      
    {!!Form::label('Número de Resolución')!!}
    {!!Form::text('numero_resolucion',null,['class'=>'form-control','placeholder'=>'Ingrese el número de la resolución', 'required'])!!}

    {!!Form::label('Fecha del Inicio del Plan de Estudio')!!}<br>
    <div class="input-group">
      <div class="input-group-addon">
        <i class="fa fa-calendar"></i>
      </div>
     {!!Form::input('date', 'fecha_inicio', null, ['class' => 'form-control','required']);!!}
    </div><!-- /.input group -->

    {!!Form::label('Fecha Fin del Plan de Estudio')!!}<br>
    <div class="input-group">
      <div class="input-group-addon">
          <i class="fa fa-calendar"></i>
      </div>
      {!!Form::input('date', 'fecha_fin', null, ['class' => 'form-control','required']);!!}
    </div><!-- /.input group -->
              
    {!!Form::label('Programa')!!}<br>
    {!!Form::select('id_programa',  $programa ,null,['class' => 'form-control select2','placeholder'=>'Seleccione el Programa','required']);!!}
</div>