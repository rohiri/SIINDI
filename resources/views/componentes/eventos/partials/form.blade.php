<div class="form-group">
    {!!Form::label('Numero del Consejo')!!}
    {!!Form::text('numero_consejo',null,['class'=>'form-control','placeholder'=>'Ingrese el número del consejo','required'])!!}
    
    {!!Form::label('Fecha')!!}<br>
    <div class="input-group">
        <div class="input-group-addon">
          <i class="fa fa-calendar"></i>
        </div>
        {!!Form::input('date', 'fecha', null, ['class' => 'form-control','placeholder'=>'s','required']);!!}
    </div>
                            
    {!!Form::label('Nombre del Evento')!!}
    {!!Form::text('nombre_evento',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del evento','required'])!!}

    {!!Form::label('Descripción del Evento')!!}
    {!!Form::text('descripcion_evento',null,['class'=>'form-control','placeholder'=>'Ingrese la descripción del evento'])!!}

    {!!Form::label('Lugar')!!}
    {!!Form::text('lugar',null,['class'=>'form-control','placeholder'=>'Ingrese el lugar del evento'])!!}

    {!!Form::label('Caracter Evento')!!}
    {!!Form::select('caracter_evento', array(
        'Nacional'    => 'Nacional',
        'Internacional'   => 'Internacional')
        ,null,['class'=>'form-control select2','placeholder'=>'','required'])!!}<br>
    

    {!!Form::label('Tipo de Evento')!!}<br>
    {!!Form::select('id_tipoeventos',  $tipo_evento,null,['class'=>'form-control select2','placeholder'=>'Seleccione el Tipo de Evento','required'] )!!}
</div>