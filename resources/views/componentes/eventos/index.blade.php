@extends('layaouts.tablas')
@section('content')
  <section class="content"> 
    @include('componentes.eventos.partials.modal') 
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            @include('layaouts.partials.mensaje')
            <h3 class="box-title">Listado Eventos</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="row form-group">
              <div class="col-md-3">
                <a href="{!! URL('evento/create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Nuevo Evento</a>
              </div>
            </div>
            <table id="example3" class="table table-bordered table-striped">
              <thead>
                <th>N. Consejo</th>
                <th>Fecha</th>
                <th>Nombre Evento</th>
                <th>Descripcion</th>
                <th>Lugar</th>
                <th>Tipo Evento</th>
                <th>Caracter Evento</th>
                <th>Acciones</th>
              </thead>
              <tbody>
                @foreach($evento as $t)
                  <tr>
                    <td>{{ $t->numero_consejo}}</td>
                    <td>{{ $t->fecha}}</td>
                    <td>{{ $t->nombre_evento}}</td>
                    <td>{{ $t->descripcion_evento}}</td>
                    <td>{{ $t->lugar}}</td>
                    <td>{{ $t->tipo->nombre_tipoevento}}</td>
                    <td>{{ $t->caracter_evento}}</td>
                    <td>
                      {!! link_to_route('evento.edit', $title='Editar', $parameters=$t->id, $atrributes=['class'=>'btn btn-warning']) !!}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>   
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div>                  
  </section><!-- /.content -->
@endsection