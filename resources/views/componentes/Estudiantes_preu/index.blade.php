@extends('layaouts.tablas')
@section('content')
  <section class="content"> 
    @include('componentes.Estudiantes_preu.partials.modal')  
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            @include('layaouts.partials.mensaje')
            <h3 class="box-title">Listado Estudiantes (Matriculados, Egresados, Deserción)</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="row form-group">
              <div class="col-md-3">
                <a href="{!! URL('secretaria-estudiante/create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Nuevo Estudiante</a>
              </div>
            </div>
            <table id="example3" class="table table-bordered table-striped">
              <thead>
                <th>Periodo</th>
                <th>Inscritos</th>
                <th>Admitidos</th>
                <th>M.T</th>
                <th>M.P.S</th>
                <th>Graduados</th>
                <th>Retirados</th>
                <th>T.D</th>
                <th>T.C</th>
                <th>Acciones</th>
              </thead>
              <tbody>
                @foreach($estudiantes as $e)
                  <tr> 
                    <td>{{$e->NombrePeriodo}}</td>
                    <td>{{$e->e_inscritos}}</td>
                    <td>{{$e->e_admintidos}}</td>
                    <td>{{$e->matriculados_total}}</td>
                    <td>{{$e->matriculados_primersemestre}}</td>
                    <td>{{$e->graduados}}</td>
                    <td>{{$e->retirados}}</td>
                    <td>{{$e->tasa_desercion.""."%" }}</td>
                    <td>{{$e->tasa_culminacion.""."%"}}</td>
                    <td>
                      {!! link_to_route('secretaria-estudiante.edit', $title='Editar', $parameters=$e->id, $atrributes=['class'=>'btn btn-warning']) !!}
                      
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            <div class="row">
              <div class="col-xs-12">
                <ul>
                  <li>M.T = Matriculados Total</li>
                  <li>M.P.S = Matriculados Primer Semestre</li>
                  <li>T.D = Tasa de Deserción</li>
                  <li>T.C = Tasa de Culminación de Carrera</li>
                </ul>
              </div>
            </div> 
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div><!-- /.row -->                
  </section><!-- /.content -->
@endsection