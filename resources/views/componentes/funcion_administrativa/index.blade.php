@extends('layaouts.tablas')
@section('content')
  <section class="content"> 
    @include('componentes.funcion_administrativa.partials.modal') 
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            @include('layaouts.partials.mensaje')
            <h3 class="box-title">Listado de Funciones Administrativas</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="row form-group">
              <div class="col-md-3">
                <a href="{!! URL('funciones-administrativas/create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Nueva Funcion Administrativa</a>
              </div>
            </div>
            <table id="example3" class="table table-bordered table-striped">
              <thead>
                <th>Nombre Función</th>
                <th>Sesion Consejo</th>
                <th>Fecha Sesión</th>
                <th>Horas Funcion</th>
                <th>Acciones</th>
              </thead>
              <tbody>
                @foreach($funcion as $f)
                  <tr>
                    <td>{{ $f->nombre_funcion}}</td>
                    <td>{{ $f->sesion_consejo}}</td>
                    <td>{{ $f->fecha_sesion}}</td>
                    <td>{{ $f->max_horas}}</td>
                    <td>
                      {!! link_to_route('funciones-administrativas.edit', $title='Editar', $parameters=$f->id, $atrributes=['class'=>'btn btn-warning']) !!}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>   
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div>                  
  </section><!-- /.content -->
@endsection