<div class="form-group">      
	{!!Form::label('Profesor')!!}<br>
    {!!Form::select('id_profesor', $profesor, null,['class'=>'form-control select2', 'placeholder'=>'Seleccione el Profesor','required'])!!} <br>
    
    {!!Form::label('Periodo')!!}<br>
    {!!Form::select('id_periodo', $periodo, null, ['class'=>'form-control select2','placeholder' => 'Seleccion el Periodo','required'])!!} <br>
    
    {!!Form::label('Horas Docencia')!!}
    {!!Form::number('h_docencia',null,['class'=>'form-control','step'=>'0.01','placeholder'=>'Digite las Horas de Docencia','required'])!!}    

    {!!Form::label('Horas Investigación')!!}
    {!!Form::number('h_investigacion',null,['class'=>'form-control','step'=>'0.01','placeholder'=>'Digite las Horas de Investigación','required'])!!}    

    {!!Form::label('Horas Extensión')!!}
    {!!Form::number('h_extension',null,['class'=>'form-control','step'=>'0.01','placeholder'=>'Digite las Horas de Extensión ','required'])!!}    

    {!!Form::label('Horas Administrativo')!!}
    {!!Form::number('h_administrativo',null,['class'=>'form-control','step'=>'0.01','placeholder'=>'Digite las Horas de Administrativo','required'])!!}

    {!!Form::label('Otras')!!}
    {!!Form::number('h_otras',null,['class'=>'form-control','step'=>'0.01','placeholder'=>'Digite las Otras Horas','required'])!!}    
</div>