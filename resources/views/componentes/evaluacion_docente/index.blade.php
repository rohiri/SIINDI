@extends('layaouts.tablas')
@section('content')
  <section class="content"> 
    @include('componentes.evaluacion_docente.partials.modal')  
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            @include('layaouts.partials.mensaje')
            <h3 class="box-title">Listado de Evaluaciones Docentes</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="row form-group">
              <div class="col-md-3">
                <a href="{!! URL('evaluacion-docente/create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Nueva Evaluación</a>
              </div>
            </div>
            <table id="example3" class="table table-bordered table-striped">
              <thead>
                <th>Profesor</th>
                <th>Año</th>
                <th>Semestre</th>
                <th>Valor</th>
                <th>Nota</th>
                <th>Acciones</th>
              </thead>
              <tbody>
                @foreach($evaluacion as $e)
                  <tr> 
                    <td>{{ $e->getProfesor->primer_nombre." ".$e->getProfesor->segundo_nombre." ".$e->getProfesor->primer_apellido." ".$e->getProfesor->segundo_apellido}}</td>
                    <td>{{ $e->getPeriodo->anio}}</td>
                    <td>{{ $e->getPeriodo->periodo}}</td>
                    <td>{{ $e->valor}}</td>
                    @if($e->nota=="Insuficiente")
                      <td><span class="label label-danger">{{$e->nota}}</span></td>
                    @elseif($e->nota=="Aceptable")
                      <td><span class="label label-warning">{{$e->nota}}</span></td>
                    @elseif($e->nota=="Sobresaliente")
                      <td><span class="label label-primary">{{$e->nota}}</span></td>
                    @elseif($e->nota=="Excelente")
                      <td><span class="label label-success">{{$e->nota}}</span></td>
                    @endif
                    <td>
                      {!! link_to_route('evaluacion-docente.edit', $title='Editar', $parameters=$e->id, $atrributes=['class'=>'btn btn-warning']) !!}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>   
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div><!-- /.row -->                
  </section><!-- /.content -->
@endsection