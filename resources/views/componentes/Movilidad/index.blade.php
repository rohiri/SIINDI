@extends('layaouts.tablas')
@section('content')
  <section class="content"> 
    @include('componentes.Movilidad.partials.modal')  
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            @include('layaouts.partials.mensaje')
            <h3 class="box-title">Listado Movilidades Docentes</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="row form-group">
              <div class="col-md-3">
                <a href="{!! URL('movilidad-docente/create') !!}" class="btn btn-success"><i class="fa fa-plus"></i>Nueva Movilidad</a>
              </div>
            </div>
            <table id="example3" class="table table-bordered table-striped">
              <thead>
                <th>Profesor</th>
                <th>Periodo</th>
                <th>Evento</th>
                <th>Caracter Evento</th>
                <th>Acciones</th>
              </thead>
              <tbody>
                @foreach($movilidades as $m)
                  <tr> 
                    <td>{{ $m->Profesor->NombreProfesores}}</td>
                    <td>{{ $m->Periodo->NombrePeriodo}}</td>
                    <td>{{ $m->Evento->nombre_evento}}</td>
                    <td>{{ $m->Evento->caracter_evento}}</td>
                    <td>
                      {!! link_to_route('movilidad-docente.edit', $title='Editar', $parameters=$m->id, $atrributes=['class'=>'btn btn-warning']) !!}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div><!-- /.row -->                
  </section><!-- /.content -->
@endsection