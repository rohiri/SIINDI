<div class="form-group">                           
    {!!Form::label('Tipo Movilidad')!!}
    {!!Form::select('tipo_movilidad', array(
        'Profesor'    => 'Profesor',
        'Estudiante'   => 'Estudiante')
        ,null,['class'=>'form-control select2','placeholder'=>'','required'])!!}<br>

    {!!Form::label('Profesor')!!}<br>
    {!!Form::select('id_profesor', $profesor, null,['class'=>'form-control select2', 'placeholder'=>'Seleccione el Profesor','required'])!!} <br>
    
    {!!Form::label('Periodo')!!}<br>
    {!!Form::select('id_periodo', $periodo, null, ['class'=>'form-control select2','placeholder' => 'Seleccion el Periodo','required'])!!} <br>

    {!!Form::label('Evento')!!}<br>
    {!!Form::select('id_evento', $evento, null, ['class'=>'form-control select2','placeholder' => 'Seleccion el Evento','required'])!!} <br>
</div>