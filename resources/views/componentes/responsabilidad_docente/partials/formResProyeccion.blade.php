<div class="form-group">    
	{!!Form::label('Proyecto de Proyeccion Social')!!}<br>
    {!!Form::Select('id_proyectopro', $proyecto, null,['class'=>'form-control select2', 'placeholder'=>'Seleccione El Proyecto de Proyeccion Social','required'])!!} <br>

    {!!Form::label('Horas Semanales')!!}<br>
    {!!Form::number('horasp_semanal', null,['class'=>'form-control horas', 'placeholder'=>'Digite las Horas de Investigacion del Profesor','min'=>'0','required'])!!} <br>                            

</div>
