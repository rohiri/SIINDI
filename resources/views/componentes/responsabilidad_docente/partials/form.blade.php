<div class="form-group">                           
    {!!Form::label('Profesor')!!}<br>
    {!!Form::select('id_profesor', $profesor, null,['class'=>'form-control select2 required', 'placeholder'=>'Seleccione el Profesor','required'])!!} <br>                   
    
    {!!Form::label('Curso')!!}<br>
    {!!Form::select('id_grupo',$grupo,null,['class'=>'form-control select2 required','placeholder'=>'Seleccione el curso','required'])!!}<br>
    
    {!!Form::label('Vinculación')!!}<br>
    {!!Form::select('id_vinculacion',$vinculacion,null,['class'=>'form-control select2 required','placeholder'=>'Ingrese el nombre el tipo de vinculación','required','id'=>'id_vinculacion'])!!}<br>
    
    {!!Form::label('Numero de Estudiantes')!!}<br>
    {!!Form::Number('numero_estudiantes',null,['class'=>'form-control required','placeholder'=>'Ingrese la cantidad de estudiantes','min'=>'0','required'])!!}
    
    {!!Form::label('Horas Directas')!!}<br>
    {!!Form::Number('horas_directas',null,['class'=>'form-control required horas','placeholder'=>'Ingrese la cantidad de horas directas','min'=>'0','required'])!!}
    
    {!!Form::label('Horas de Tutoria')!!}<br>
    {!!Form::Number('horas_tutoria',null,['class'=>'form-control required horas','placeholder'=>'Ingrese la cantidad de horas de tutoria','min'=>'0','required'])!!}
    
    {!!Form::label('Horas de Preparación')!!}<br>
    {!!Form::Number('horas_preparacion',null,['class'=>'form-control required horas','placeholder'=>'Ingrese la cantidad de horas reparación','min'=>'0','required'])!!}
    
    {!!Form::label('Número de Semanas')!!}<br>
    {!!Form::Number('numero_semanas',null,['class'=>'form-control required','placeholder'=>'Ingrese el número de semanas','min'=>'1','required'])!!}
</div>

