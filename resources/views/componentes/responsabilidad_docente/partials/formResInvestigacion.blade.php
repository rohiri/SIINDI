<div class="form-group">           
	{!!Form::label('Proyecto de Investigacion')!!}<br>
    {!!Form::Select('id_proyectoinv', $proyecto, null,['class'=>'form-control select2', 'placeholder'=>'Seleccione El Proyecto de Investigacion','required'])!!} <br>

    {!!Form::label('Horas Semanales')!!}<br>
    {!!Form::number('horasi_semanal', null,['class'=>'form-control horas', 'placeholder'=>'Digite las Horas de Investigacion del Profesor','min'=>'0','required'])!!} <br>     
</div>

