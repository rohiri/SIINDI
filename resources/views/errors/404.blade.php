@extends('layaouts.tablas')
@section('content')
 <section class="content">
          <div class="error-page">
            <h2 class="headline text-yellow"> 404</h2>
            <div class="error-content">
              <h3><i class="fa fa-warning text-yellow"></i> Oops! Página no encontrada.</h3>
              <p>
                No encontramos la página que estabas buscando.
                Puedes retornar a una opción del menu.
              </p>
            </div><!-- /.error-content -->
          </div><!-- /.error-page -->
        </section><!-- /.content -->
@endsection