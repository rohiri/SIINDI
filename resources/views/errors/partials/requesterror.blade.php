    @if($errors->has())
        <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <!--recorremos los errores en un loop y los mostramos-->
            <h4 class="header smaller lighter red">Por Favor Corrige Los Errores</h4>
            <ul>
            @foreach ($errors->all('<p>:message</p>') as $message)
                <li>{!! $message !!}</li>
            @endforeach
            </ul>
        </div>
    @endif
