@extends('layaouts.newtablas')
@section('content')

<section class="content">
  @if(\Session::get("mensaje"))
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {!! \Session::get("mensaje") !!}
    </div>
  @endif
  <div class="box">
    <div class="box-header">
      <h2><i class="fa fa-question"></i><b>Preguntas</b></h2>
    </div>
    {!! Form::open(array("url"=>URL("preguntas/create"),"method"=>"POST")) !!}
      <div class="box-content">
        <div class="row"> 
          <div class="col-md-4"> 
            {!! Form::select("periodo",$periodos,null,array("class"=>"form-control")) !!}
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
              <center><button type="submit" class="btn btn-success"> Guardar</button></center>
          </div>
        </div>
      </div>
    {!! Form::close() !!}
  </div>
</section>   
@endsection
@section('js-adicional')
    <script>
        $(document).ready(function(){
            generarTabla();
        }); 
        function agregarFila(){
          var cantidad = $("#preguntas tbody tr").size();
          $("#preguntas tbody").append("<tr><td class='no-edit'>"+(cantidad+1)+'</td><td></td><td><button type="button" class="btn btn-danger" onclick="quitarFila(this);"><i class="fa fa-times"></i></tr>');
          generarTabla();
        }
        function generarTabla(){
          var tabla = $('#preguntas').editableTableWidget();
          $("#preguntas tbody tr td").change(function(){
            generarTabla();
          });
          var filas = $("#preguntas tbody tr");
          var preguntas=[];
          var cont=0;
          filas.each(function(){
            preguntas[cont]=[];
            preguntas[cont][0]=$(this).children().eq(0).text();
            preguntas[cont][1]=$(this).children().eq(1).text();
            cont++;
          });
          $("#preg").val(JSON.stringify(preguntas));
        }
        function quitarFila(elemento){
          var fila = $(elemento).parent().parent();
          fila.remove();
        }
    </script>
@stop