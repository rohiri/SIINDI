@extends('layaouts.newtablas')
@section('content')
<section class="content">
    <div class="row"> 
        <div class="col-md-6">
            <div class="box">
                <div class="box-header" data-original-title>
                    <h2><i class="fa fa-ambulance"></i></i><span class="break"></span><b>Agregar Rotación al Curso</b></h2>
                    <div class="box-icon">
                    <a href="#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                </div>
                <div class="box-content">
                    {!!Form::open($form_data) !!}  
                        <div class="form-group">
                            
                                {!! Form::label('Nombre rotación')!!}
                                {!!Form::Text('nombre',$rotaciones->nombre,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}
                            
                        </div>

                        <div class="form-group">

                            <label class="control-label" for="selectError">Seleccionar curso</label>
                            {!!Form::select('curso', $cursos,$rotaciones->curso,array("class"=>"form-control"))!!}
                        
                        </div>


                       
                        

                        <div class="row form-group">
                            <div class="col-md-12">
                                {!!Form::submit('Guardar',['class'=>'btn btn-success'])!!}
                            </div>     
                        </div>
                        
                                              
                                         
                    {!! Form::close() !!}
                    
                </div>
            </div>
        </div>
    </div>

</section>

@endsection