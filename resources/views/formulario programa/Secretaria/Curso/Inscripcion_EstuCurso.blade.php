@extends('layaouts.newtablas')    
@section('content')
<section class="content">
  <div class="row"> 
    <div class="col-md-12">
      <div class="box">
        <div class="box-header" data-original-title>
          <h2><i class="fa fa-user-plus"></i><span class="break"></span><b>Inscripción Estudiante al Curso</b></h2>
          <div class="box-icon">
            <a href="#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
          </div>
        </div>
        <div class="box-content"> 
          {!! Form::open($route) !!}
            <div class="row form-group">
              <div class="col-md-12">
                {!! Form::label('Estudiante')!!}
                {!!Form::select("id_estudiante",$estudiantes,$inscripcion->id_estudiante,["class"=>"form-control","data-rel"=>"chosen"])!!}
              </div>
            </div>
            <div class="row form-group">
              <div class="col-md-12">
                {!! Form::label('Grupo')!!}
                {!!Form::select("id_grupo",$grupos,$inscripcion->id_grupo,["class"=>"form-control","data-rel"=>"chosen"])!!}
              </div>
            </div>
            <div class="row form-group">
              <div class="col-md-12">
                {!!Form::submit('Guardar',['class'=>'btn btn-success'])!!}
              </div>     
            </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</section>
@endsection