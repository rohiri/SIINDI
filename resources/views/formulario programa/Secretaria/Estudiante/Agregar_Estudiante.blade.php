@extends('layaouts.newtablas')    
@section('content')
    <section class="content">
    @if(\Session::get("mensaje-error"))
    <div class="alert alert-danger alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {!! \Session::get("mensaje-error") !!}
    </div>
  @endif
      <div class="row"> 
      <div class="col-lg-12">
      
          <div class="box">
            <div class="box-header" data-original-title>
              <h2><i class="fa fa-user-md"></i><span class="break"></span><b>Agregar Estudiante</b></h2>
              <div class="box-icon">
              
                <a href="#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                
              </div>
            </div>

            <div class="box-content">
           {!! Form::open($route) !!}
                  <div class="form-group">
                  
            {!! Form::label('Primer nombre')!!}
            {!!Form::Text('primer_nombre',$estudiante->primer_nombre,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}
                  
            {!! Form::label('Segundo nombre')!!}
            {!!Form::Text('segundo_nombre',$estudiante->segundo_nombre,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}

            {!! Form::label('Primer apellido')!!}
            {!!Form::Text('apellido_paterno',$estudiante->apellido_paterno,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}
                  
            {!! Form::label('Segundo apellido')!!}
            {!!Form::Text('apellido_materno',$estudiante->apellido_materno,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}
                  
             {!! Form::label('Numero documento')!!} 
            {!!Form::number('numero_documento',$estudiante->numero_documento,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}
             
               

            {!! Form::label('Codigo de estudiante')!!}
            {!!Form::number('codigo_estudiante',$estudiante->codigo_estudiante,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}

            {!!Form::label('Correo')!!}<br>
            {!!Form::text('email',$estudiante->email,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}

            
                  
                  </div>

                  <div class="form-group">
            {!!Form::submit('Guardar',['class'=>'btn btn-success'])!!}
                  </div>
                 


            

         
              
          
                  {!! Form::close() !!}
                         
            </div> 
            </div>      
          
          
        
      
      </div><!--/row-->
    </div>

  </section><!-- /.content -->
  @endsection