@extends('layaouts.newtablas')    
@section('content')



    <section class="content">
    @if(\Session::get("mensaje-error"))
    <div class="alert alert-danger alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {!! \Session::get("mensaje-error") !!}
    </div>
  @endif
      {!! Form::open($route) !!}
      <div class="row"> 
      <div class="col-lg-4">
      
          <div class="box">
            <div class="box-header" data-original-title>
              <h2><i class="fa fa-heartbeat"></i><span class="break"></span><b>Agregar Periodo Academico</b></h2>
              <div class="box-icon">
              
                <a href="#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                
              </div>
            </div>

            <div class="box-content">
              <form class="form-horizontal">


            {!! Form::label('Año')!!}
            {!!Form::number('anio',$periodo->anio,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}

            <div class="form-group">
            <label class="control-label" for="selectError">Seleccionar el periodo academico</label>
            {!!Form::select('periodo',["I"=>"I","II"=>"II"],$periodo->periodo,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}
            </div>


           

            


            <div class="form-group">
            {!!Form::submit('Guardar',['class'=>'btn btn-success'])!!}
                  </div>



              </form>
              </div>
              </div>
              </div>
              </div>
              {!! Form::close() !!}
    </section>
@endsection