@extends('layaouts.newtablas')

   @if(Session::has('message'))
      <div class="alert alert-danger alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         {{ Session::get('message')}}
      </div>
   @endif
@section('content')
   <section class="content">  
   @if(\Session::get("mensaje"))
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {!! \Session::get("mensaje") !!}
    </div>
  @endif
  @if(\Session::get("mensaje-error"))
    <div class="alert alert-danger alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {!! \Session::get("mensaje-error") !!}
    </div>
  @endif
  @foreach($periodos as $p) 
    <div class="modal fade" id="modalBorrar{!! $p->id !!}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Eliminar Registro</h4>
            </div>
            <div class="modal-body">
              <p>Esta seguro de eliminar el registro seleccionado?</p>
            </div>
            <div class="modal-footer">
              {!! Form::open(array("route"=>array("periodo.destroy",$p->id),"method"=>"DELETE")) !!}
                <button type="submit" class="btn btn-success">Si</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
              {!! Form::close() !!}
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
  @endforeach
      <div class="row">
            <div class="col-xs-12">
              <div class="box">
               <div class="box-header">
                  <h2 class="box-title"><i class="fa fa-heartbeat"></i><b>Listado de Periodos</b></h2>
               </div><!-- /.box-header -->
               <div class="box-body">
                  <div class="row form-group">
                    <div class="col-md-3">
                      <a href="{!! URL('periodo/create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Crear Nuevo Periodo</a>
                    </div>
                  </div>
                   <table id="example3" class="table table-bordered table-striped">
                     <thead>
                        <th>Año</th>
                        <th>Periodo</th>
                       <!-- <th>Acción</th> -->
                     </thead>
                     <tbody>
                        @foreach($periodos as $p)     
                          <tr>       
                            <td>{{ $p->anio}}</td>
                            <td>{{ $p->periodo}}</td>
                            
                           <!-- <td>
                               {!! link_to_route('periodo.edit', $title='Editar', $parameters=$p->id, $atrributes=['class'=>'btn btn-warning']) !!}
                                <button type="button" class="btn btn-danger" onclick="$('#modalBorrar{!! $p->id !!}').modal();">Borrar</button>
                            </td> -->
                          </tr>                
                        @endforeach
                      </tbody>
                  </table>   
               </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
      </div>                  

   
  </section><!-- /.content -->
   
@endsection
@section("js-adicional")
    <!-- DATA TABES SCRIPT-->
   {!!Html::script('assets/plugins/datatables/jquery.dataTables.min.js')!!}
   {!!Html::script('assets/plugins/datatables/dataTables.bootstrap.min.js')!!}
   <script type="text/javascript">
       $(function () {
           $('#example3').DataTable({
               language: {
                   lengthMenu:        "Mostrar _MENU_ Registros por Pagina",
                   search:            "Buscar ",
                   searchPlaceholder: "Busca por columnas",
                   info:              "Mostrar _START_ A _END_ De _TOTAL_ Registros",
                   zeroRecords:       "Ningun Registro Encontrado",
                   infoEmpty:         "No Hay Registros Disponibles",
                   paginate: {
                       first:         "Primero",
                       previous:      "<< Anterior",
                       next:          "Siguiente >>",
                       last:          "Ultimo"
                   }
               }
           });
       });
   </script>
@stop