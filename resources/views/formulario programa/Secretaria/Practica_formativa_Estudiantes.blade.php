@extends('layaouts.newtablas')
@section('content')

    <section class="content">
      <div class="row"> 
      <div class="col-lg-12">
      
          <div class="box">
            <div class="box-header" data-original-title>
              <h2><i class="fa fa-book"></i><span class="break"></span>DATOS GENERALES PRÁCTICA FORMATIVA</h2>
              <div class="box-icon">
              
                <a href="#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                
              </div>
            </div>

            <div class="box-content">
              <form class="form-horizontal">
                  {!!Form::open() !!}  
                  <div class="form-group">
                  
            {!! Form::label('Programa')!!}
            {!!Form::Text('programa',null,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}
                  
            {!! Form::label('Curso')!!}
            {!!Form::Text('curso',null,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}
                  
             {!! Form::label('Periodo academico')!!} <br>
            {!!Form::select('programa',['I Periodo academico', 'II Periodo Academico'])!!} <br>
             
               

            {!! Form::label('Año')!!}
            {!!Form::Text('anio',null,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}

            {!!Form::label('Semestre')!!}<br>
            {!!Form::select('semestre', ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X'])!!}<br>

                   
            {!! Form::label('Nombre docente coordinador del curso')!!}
            {!!Form::Text('docente_orientador',null,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}

                  </div>
                  
            
                  </div>  
                     
                  

                  

                 

                
            
            
          
            <div class="box-header" data-original-title>
              <h2><i class="fa fa-book"></i><span class="break"></span>GRUPO DE ESTUDIANTES A</h2>
              <div class="box-icon">
                
                <a href="#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                
              </div>
            </div>
            <div class="box-content">
              <table class="table table-striped table-bordered bootstrap-datatable datatable" id="tablaestA">
                <thead>
                  <tr>
                    <th>Apellidos y nombres estudiantes</th>
                    <th>Codigo</th>
                    
                  </tr>
                </thead>   
                
                <tr>
                
                  
                  
                  <td class="center"></td>
                  <td class="center"></td>
                  
                </tr>
                
                
              </table> 
              
                                       
           
          </div>


          
            <div class="box-header" data-original-title>
              <h2><i class="fa fa-book"></i><span class="break"></span>GRUPO DE ESTUDIANTES B</h2>
              <div class="box-icon">
                
                <a href="#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                
              </div>
            </div>
            <div class="box-content">
              <table class="table table-striped table-bordered bootstrap-datatable datatable" id="tablaestB">
                <thead>
                  <tr>
                    <th>Apellidos y nombres estudiantes</th>
                    <th>Codigo</th>
                    
                  </tr>
                </thead>   
                <tbody>
                <tr>
                
                  
                  
                  <td class="center"></td>
                  <td class="center"></td>
                  
                </tr>
                
                
              </table> 
              
            <div class="form-group">
            {!!Form::submit('Guardar',['class'=>'btn btn-primary'])!!}
                  </div> 
                  {!! Form::close() !!}       
            
          </div>
        
      </div>
      </div><!--/row-->
    </div>
  </section><!-- /.content -->
  
@endsection
@section('js-adicional')
    <script>
        $(document).ready(function(){
            var tabla = $('#tablaestA').editableTableWidget();
            var tabla = $('#tablaestB').editableTableWidget();
        }); 
    </script>
@stop