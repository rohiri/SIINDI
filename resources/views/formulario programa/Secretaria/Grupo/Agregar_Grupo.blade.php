@extends('layaouts.newtablas')    
@section('content')



    <section class="content">
      {!! Form::open($form_data) !!}
      <div class="row"> 
      <div class="col-lg-6">
      
          <div class="box">
            <div class="box-header" data-original-title>
              <h2><i class="fa fa-users"></i><span class="break"></span><b>Agregar Grupo</b></h2>
              <div class="box-icon">
              
                <a href="#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                
              </div>
            </div>

            <div class="box-content">
              <form class="form-horizontal">


            {!! Form::label('Número de grupo')!!}
            {!!Form::text('codigo',$grupo->codigo,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}

                  <div class="form-group">
                  <label class="control-label" for="selectError">Seleccionar curso</label>
                    {!!Form::select('id_curso', $cursos,$grupo->id_curso,array("class"=>"form-control"))!!}
                  </div>
            

            {!!Form::label('Periodo academico')!!}<br>
            {!!Form::select('id_periodo', $periodos,$grupo->id_periodo,array("class"=>"form-control"))!!} <br>
            <br>

            


            <div class="form-group">
            {!!Form::submit('Guardar',['class'=>'btn btn-success'])!!}
                  </div>



              </form>
              </div>
              </div>
              </div>
              </div>
              {!! Form::close() !!}
              </section>




@endsection