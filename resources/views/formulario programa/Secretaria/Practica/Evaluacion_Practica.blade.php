@extends('layaouts.newtablas')    
@section('content')
<section class="content">
  @if(\Session::get("mensaje"))
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {!! \Session::get("mensaje") !!}
    </div>
  @endif
  <div class="row"> 
    <div class="col-md-12">
      <div class="box">
        <div class="box-header" data-original-title>
          <h2><i class="fa fa-stethoscope"></i></i><span class="break"></span><b>Evaluación de Practica Formativa</b></h2>
          <div class="box-icon">
            <a href="#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
          </div>
        </div>
        <div class="box-content"> 
          {!! Form::open($route) !!}
            <div class="row form-group">
              <div class="col-md-12">
                {!! Form::label('Estudiante')!!}
                {!!Form::select("id_estudiante",$estudiantes,null,["class"=>"form-control","data-rel"=>"chosen"])!!}
              </div>
            </div>
            <div class="row form-group">
              <div class="col-md-12">
                {!! Form::label('Rotación')!!}
                {!!Form::select("id_rotacion",$rotaciones,null,["class"=>"form-control","data-rel"=>"chosen"])!!}
              </div>
            </div>
            <div class="row form-group">
              <div class="col-md-12">
                {!!Form::submit('Guardar',['class'=>'btn btn-success'])!!}
              </div>     
            </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</section>
@endsection