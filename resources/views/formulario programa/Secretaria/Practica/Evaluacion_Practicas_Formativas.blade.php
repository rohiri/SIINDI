@extends('layaouts.newtablas')
@section('content')
{!!Form::open($route) !!} 
<section class="content">
    <div class="row"> 
        <div class="col-md-12">
            <div class="box">

              <div class="box-header" data-original-title>
                  <h2><i class="fa fa-stethoscope"></i></i><b>Evaluación de las Practicas Formativas</b></h2>
              </div>

                <div class="box-content">
                    <div id="MyWizard" class="wizard">
                        <ul class="steps">

                          <li data-target="#step1" class="active"><span class="badge badge-info">1</span></li>
                          <li data-target="#step2"><span class="badge">2</span></li>
                          <li data-target="#step3"><span class="badge">3</span></li>
                          <li data-target="#step4"><span class="badge">4</span></li>
                          <li data-target="#step5"><span class="badge">5</span></li>
                                        
                        </ul>

                        <div class="actions">
                            <button type="button" class="btn btn-prev"> <i class="fa fa-arrow-left"></i>Anterior</button>
                            <button type="button" class="btn btn-success btn-next" data-last="Finish">Siguiente<i class="fa fa-arrow-right"></i></button>
                        </div>
                    </div>


                    <div class="step-content">
                        <div class="step-pane active" id="step1">
                                    <div class="box-content">
                                                <h2><b><center>DIMENSIÓN COMPETENCIA COGNITIVA</center></b></h2>
                                                <table class="table" id="preguntas1">
                                                    <thead>
                                                    <tr>
                                                    <th>INDICADORES DE DESEMPEÑO</th>
                                                    <th style="width:150px;">PUNTAJE</th>
                                                    <th style="width:50px;">QUITAR</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                      @foreach($rot1 as $r)
                                                        <tr>
                                                          <td>{!! $r->indicador !!}</td>
                                                          <td>{!! $r->puntaje !!}</td>
                                                          <td><button type="button" class="btn btn-danger" onclick="quitarFila(this,1);"><i class="fa fa-times"></i></button></td>
                                                        </tr>
                                                      @endforeach
                                                    </tbody>
                                                </table>
                                                <button type="button" class="btn btn-success" onclick="agregarFila(1);"><i class="fa fa-plus"></i> Agregar Indicador</button>
                                                 <br>
                                                 <br>
                                                                          
                                                  <label>Puntaje para la dimensión</label>
                                                  <input type="text" class="form-control"name="puntaje1" id="puntaje1" value="{!! $puntaje1 !!}">                         
                                                    <label class="control-label" for="observaciones"> OBSERVACIONES ESPECIFICAS</label>
                                                    
                                                <textarea id="observaciones1" name="observaciones1"
                                                    rows="1"
                                                    style="width: 100%;height: 150px;resize: none;"
                                                    maxlength="1000">{!! $observaciones1 !!}
                                                </textarea>
                                                
                                    </div>
                        </div>

                        <div class="step-pane" id="step2">
                        <div class="box-content">

                                                <h2><b><center>DIMENSIÓN COMPETENCIA PRAXIOLÓGICA</center></b></h2>
                                                <table class="table" id="preguntas2">
                                                    <thead>
                                                    <tr>
                                                    <th>INDICADORES DE DESEMPEÑO</th>
                                                    <th style="width:150px;">PUNTAJE</th>
                                                    <th style="width:50px;">QUITAR</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                      @foreach($rot2 as $r)
                                                        <tr>
                                                          <td>{!! $r->indicador !!}</td>
                                                          <td>{!! $r->puntaje !!}</td>
                                                          <td><button type="button" class="btn btn-danger" onclick="quitarFila(this,2);"><i class="fa fa-times"></i></button></td>
                                                        </tr>
                                                      @endforeach
                                                    </tbody>
                                                </table>
                                                <button type="button" class="btn btn-success" onclick="agregarFila(2);"><i class="fa fa-plus"></i> Agregar Indicador</button>
                                                <br>
                                                <br>
                                                <label>Puntaje para la dimensión</label>
                                                <input type="text" class="form-control"name="puntaje2" id="puntaje2" value="{!! $puntaje2 !!}">
                      <label class="control-label" for="observaciones">OBSERVACIONES ESPECÍFICAS:</label>
                      <div class="controls">
                        <textarea id="observaciones2" name="observaciones2"
                                  rows="1"
                                  style="width: 100%;height: 150px;resize: none;"
                                  maxlength="1000">{!! $observaciones2 !!}
                                  </textarea>
                          </div>
                        </div>                    
                      </div>

                      <div class="step-pane" id="step3">
                        <div class="box-content">

                                                <h2><b><center>DIMENSIÓN COMPETENCIA ACTITUDINAL</center></b></h2>
                                                <table class="table" id="preguntas3">
                                                    <thead>
                                                    <tr>
                                                    <th>INDICADORES DE DESEMPEÑO</th>
                                                    <th style="width:150px;">PUNTAJE</th>
                                                    <th style="width:50px;">QUITAR</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                      @foreach($rot3 as $r)
                                                          <tr>
                                                            <td>{!! $r->indicador !!}</td>
                                                            <td>{!! $r->puntaje !!}</td>
                                                            <td><button type="button" class="btn btn-danger" onclick="quitarFila(this,3);"><i class="fa fa-times"></i></button></td>
                                                          </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                <button type="button" class="btn btn-success" onclick="agregarFila(3);"><i class="fa fa-plus"></i> Agregar Indicador</button>
                                                <br>
                                                <br>
                                                <label>Puntaje para la dimensión</label>
                                                <input type="text" class="form-control" name="puntaje3" id="puntaje3" value="{!! $puntaje3 !!}">
                      <label class="control-label" for="observaciones">OBSERVACIONES ESPECÍFICAS:</label>
                      <div class="controls">
                        <textarea id="observaciones3" name="observaciones3"
                                  rows="1"
                                  style="width: 100%;height: 150px;resize: none;"
                                  maxlength="1000">{!! $observaciones3 !!}
                                  </textarea>
                          </div>
                        </div>                    
                      </div>

                      <div class="step-pane" id="step4">
                        <div class="box-content">

                                                <h2><b><center>DIMENSIÓN COMPETENCIA COMUNICATIVA</center></b></h2>
                                                <table class="table" id="preguntas4">
                                                    <thead>
                                                    <tr>
                                                    <th>INDICADORES DE DESEMPEÑO</th>
                                                    <th style="width:150px;">PUNTAJE</th>
                                                    <th style="width:50px;">QUITAR</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                      @foreach($rot4 as $r)
                                                        <tr>
                                                          <td>{!! $r->indicador !!}</td>
                                                          <td>{!! $r->puntaje !!}</td>
                                                          <td><button type="button" class="btn btn-danger" onclick="quitarFila(this,4);"><i class="fa fa-times"></i></button></td>
                                                        </tr>
                                                      @endforeach
                                                    </tbody>
                                                </table>
                                                <button type="button" class="btn btn-success" onclick="agregarFila(4);"><i class="fa fa-plus"></i> Agregar Indicador</button>
                                                <br>
                                                <br>
                                                <label>Puntaje para la dimensión</label>
                                                <input type="text" class="form-control"name="puntaje4" id="puntaje4" value="{!! $puntaje4 !!}">
                      <label class="control-label" for="observaciones">OBSERVACIONES ESPECÍFICAS:</label>
                      <div class="controls">
                        <textarea id="observaciones4" name="observaciones4"
                                  rows="1"
                                  style="width: 100%;height: 150px;resize: none;"
                                  maxlength="1000">{!! $observaciones4 !!}
                                  </textarea>
                          </div>
                        </div>                    
                      </div>
                      <div class="step-pane" id="step5">
                        <div class="box-content">  
                          <div class="row">
                            <div class="col-md-3">
                              <label>Puntaje Total</label>  
                              <input type="text" class="form-control"name="puntajet" id="puntajet" readonly="readonly" value="{!! $puntajet !!}"> 
                            </div>
                            <div class="col-md-3">
                              <label>Nota</label>
                              <input type="text" class="form-control"name="nota" id="nota" value="{!! $nota !!}"> 
                            </div>                          
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <label class="" for="observaciones">OBSERVACIONES GENERALES:</label>
                                  <textarea id="observacionesg" name="observacionesg"
                                            rows="1"
                                            style="width: 100%;height: 150px;resize: none;"
                                            maxlength="1000" class="form-control">{!! $observacionesg !!}
                                            </textarea>
                            </div>
                          </div>
                          <div>
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
                          </div>
                        </div>                    
                      </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 
<input type="hidden" name="rotacion" id="rotacion" value="{!! $data['id_rotacion'] !!}">  
<input type="hidden" name="preg1" id="preg1">  
<input type="hidden" name="preg2" id="preg2">
<input type="hidden" name="preg3" id="preg3">
<input type="hidden" name="preg4" id="preg4">
{!!Form::close() !!} 
@endsection
@section('js-adicional')
    <script>
        $(document).ready(function(){
            generarTabla(1);
            generarTabla(2);
            generarTabla(3);
            generarTabla(4); 
            $("#puntaje1, #puntaje2, #puntaje3, #puntaje4").change(function(){
              if(!/^\d*$/.test($(this).val()))
                $(this).val(0);
              generarTabla();
            });
        }); 
        function agregarFila(tbl){
          var cantidad = $("#preguntas"+tbl+" tbody tr").size();
          $("#preguntas"+tbl+" tbody").append('<tr><td></td><td></td><td><button type="button" class="btn btn-danger" onclick="quitarFila(this,'+tbl+');"><i class="fa fa-times"></i></button></td></tr>');
          generarTabla(tbl);
        }
        function generarTabla(tbl){
          var tabla = $('#preguntas'+tbl).editableTableWidget();
          $("#preguntas"+tbl+" tbody tr td").change(function(){
            generarTabla(tbl);
          });
          var filas = $("#preguntas"+tbl+" tbody tr");
          var preguntas=[];
          var cont=0;
          filas.each(function(){
            preguntas[cont]=[];
            preguntas[cont][0]=$(this).children().eq(0).text();
            preguntas[cont][1]=$(this).children().eq(1).text();
            cont++;
          });
          $("#preg"+tbl).val(JSON.stringify(preguntas));
          $("#puntajet").val(numero($("#puntaje1").val())+numero($("#puntaje2").val())+numero($("#puntaje3").val())+numero($("#puntaje4").val()));
        }
        function numero(texto){
          if(texto=="")
            return 0;
          else
            return parseFloat(texto);
        }
        function quitarFila(elemento,tbl){
          var fila = $(elemento).parent().parent();
          fila.remove();
          generarTabla(tbl);
        }
    </script>
@stop