  @extends('layaouts.newtablas')
    @section('content')

<section class="content">
  <div class="modal fade" id="modalProfesores">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Seleccione el Docente Orientador</h4>
        </div>
        <div class="modal-body">
          {!! Form::select("docente_orientador",$profesores,null,array("class"=>"form-control","id"=>"docente_orientador")) !!}
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success" onclick="seleccionarProfesor();">Seleccionar</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <div class="modal fade" id="modalDias">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Seleccione los Dias</h4>
        </div>
        <div class="modal-body">
          <select id="selectDias" class="form-control" size="7" multiple="multiple">
            <option value="Lunes">Lunes</option>
            <option value="Martes">Martes</option>
            <option value="Miercoles">Miercoles</option>
            <option value="Jueves">Jueves</option>
            <option value="Viernes">Viernes</option>
            <option value="Sabado">Sabado</option>
            <option value="Domingo">Domingo</option>
          </select>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success" onclick="seleccionarDias();">Seleccionar</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <div class="modal fade" id="modalHorario">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Seleccione el Horario</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="lunes col-md-6">
              <h3>Lunes</h3>
              <label>Desde las:</label>
              <input type="text" class="form-control" id="timepicker1">
              <label>Hasta las:</label>
              <input type="text" class="form-control" id="timepicker2">
            </div>
            <div class="martes col-md-6">
              <h3>Martes</h3>
              <label>Desde las:</label>
              <input type="text" class="form-control" id="timepicker3">
              <label>Hasta las:</label>
              <input type="text" class="form-control" id="timepicker4">
            </div>
            <div class="miercoles col-md-6">
              <h3>Miercoles</h3>
              <label>Desde las:</label>
              <input type="text" class="form-control" id="timepicker5">
              <label>Hasta las:</label>
              <input type="text" class="form-control" id="timepicker6">
            </div>
            <div class="jueves col-md-6">
              <h3>Jueves</h3>
              <label>Desde las:</label>
              <input type="text" class="form-control" id="timepicker7">
              <label>Hasta las:</label>
              <input type="text" class="form-control" id="timepicker8">
            </div>
            <div class="viernes col-md-6">
              <h3>Viernes</h3>
              <label>Desde las:</label>
              <input type="text" class="form-control" id="timepicker9">
              <label>Hasta las:</label>
              <input type="text" class="form-control" id="timepicker10">
            </div>
            <div class="sabado col-md-6">
              <h3>Sabado</h3>
              <label>Desde las:</label>
              <input type="text" class="form-control" id="timepicker11">
              <label>Hasta las:</label>
              <input type="text" class="form-control" id="timepicker12">
            </div>
            <div class="domingo col-md-6">
              <h3>Domingo</h3>
              <label>Desde las:</label>
              <input type="text" class="form-control" id="timepicker13">
              <label>Hasta las:</label>
              <input type="text" class="form-control" id="timepicker14">
            </div>      
          </div>
        </div> 
        <div class="modal-footer">
            <button type="button" class="btn btn-success" onclick="seleccionarHorario();">Seleccionar</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <div class="modal fade" id="modalFecha">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Seleccione la Fecha</h4>
        </div>
        <div class="modal-body">
          <input type="date" class="form-control" id="fechaInput">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success" onclick="seleccionarFecha();">Seleccionar</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <div class="row"> 
    <div class="col-md-12">
      {!! Form::open($form_data) !!}    
        <div class="box">       
          <div class="box-header" data-original-title>
            <h2><i class="fa fa-stethoscope"></i><b>Practica Formativa Datos Generales</b></h2>
          </div>
          <div class="box-content">
            <div class="panel panel-default">
              <div class="panel-heading">Datos Básicos</div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-4">
                    {!! Form::label('Nombre de la practica')!!}
                    {!!Form::Text('Nombre_practica',$practica->nombre_practica,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}     
                  </div>
                  <div class="col-md-6">
                    {!! Form::label('Institución, organizacion, comunidad , escenario de practica')!!}
                    {!!Form::Text('escenario_practica',$practica->escenario_practica,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}     
                  </div>
                  <div class="col-md-2">
                    {!! Form::label('Valor total practica')!!}
                    {!!Form::Text('valor_totalpractica',$practica->valor_totalpractica,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    {!! Form::label('Objetivo de la práctica formativa')!!}
                    {!!Form::Text('objetivo_practica',$practica->objetivo_practica,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}       
                  </div>
                </div> 
                <div class="row">
                  <div class="col-md-12">
                    {!! Form::label('Competencias que desarrolla el curso')!!}
                    {!!Form::Text('competencias_curso',$practica->competencias_curso,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    {!! Form::label('Docente Coordinador')!!}
                    {!! Form::select("docente_coordinador",$profesores,$practica->docente_coordinador,array("class"=>"form-control")) !!}
                  </div>
                  <div class="col-md-6">
                    {!! Form::label('Grupo')!!}
                    {!! Form::select("id_grupo",$grupos,$practica->id_grupo,array("class"=>"form-control","id"=>"id_grupo")) !!}
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">Rotaciones</div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-12">
                    <table class="table table-striped table-bordered bootstrap-datatable datatable" id="tabladt">
                      <thead>
                        <tr>
                          <th>Id</th>
                          <th>Nombre</th>
                          <th>Ubicación</th>
                          <th>Actividades especificas</th>
                          <th>N° estudiantes por grupo</th>
                          <th>Docente Orientador</th>
                          <th>Dias</th>
                          <th>Horario</th>
                          <th>Fecha Inicio</th>
                          <th>Fecha Fin</th>
                          <th>Valor</th>
                        </tr>
                      </thead> 
                      <tbody>  
                        @if(count($rotaciones)>0)
                          @foreach($rotaciones as $r)
                            <tr>
                              <td class="no-edit">{!! $r[0] !!}</td>
                              <td class="no-edit">{!! $r[1] !!}</td>
                              <td class="">{!! $r[2] !!}</td>
                              <td class="">{!! $r[3] !!}</td>
                              <td class="number">{!! $r[4] !!}</td>
                              <td class="no-edit docente">{!! $r[5] !!}</td>                              
                              <td class="no-edit dias">{!! $r[6] !!}</td>
                              <td class="no-edit horario">{!! $r[7] !!}</td>
                              <td class="no-edit fecha">{!! $r[8] !!}</td>
                              <td class="no-edit fecha">{!! $r[9] !!}</td>
                              <td class="center">{!! $r[10] !!}</td>
                            </tr>
                          @endforeach
                        @endif
                      </tbody>
                    </table>
                    <input type="hidden" name="rotaciones" id="rotaciones" value="">
                  </div>
                </div>
              </div>
            </div>
            <center><button type="submit" class="btn btn-success"></i> Guardar</button></center>
          </div>
        </div>  
        {!! Form::close() !!}                 
    </div>
  </div>
</section>
@endsection
@section('js-adicional')
    {!!Html::script('assets2/js/moment-with-locales.js')!!}
    {!!Html::script('assets2/js/bootstrap-datetimepicker.js')!!}
    <script>
        var celdaActiva;
        $(document).ready(function(){
            generarTabla();
            $('[data-rel="chosen"],[rel="chosen"]').chosen();
            $("#id_grupo").change(function(){
              $("#tabladt tbody tr").each(function(){
                $(this).remove();
              });
              $.get("{!! URL('ajax/cargarRotaciones') !!}",{grupo:$("#id_grupo").val()},function(result){
                var rotaciones=JSON.parse(result);
                for(var i=0;i<rotaciones.length;i++){                  
                  $("#tabladt tbody").append('<tr><td class="no-edit">'+rotaciones[i][0]+'</td><td class="no-edit">'+rotaciones[i][1]+'</td><td></td><td></td><td class="number"></td><td class="no-edit docente"></td><td class="no-edit dias"></td><td class="no-edit horario"></td><td class="no-edit fecha"></td><td class="no-edit fecha"></td><td></td></tr>');
                }
                generarTabla();
              });
            });
            $('#timepicker1').datetimepicker({
                format: 'LT'
            });
            $('#timepicker2').datetimepicker({
                format: 'LT'
            });
            $('#timepicker3').datetimepicker({
                format: 'LT'
            });
            $('#timepicker4').datetimepicker({
                format: 'LT'
            });
            $('#timepicker5').datetimepicker({
                format: 'LT'
            });
            $('#timepicker6').datetimepicker({
                format: 'LT'
            });
            $('#timepicker7').datetimepicker({
                format: 'LT'
            });
            $('#timepicker8').datetimepicker({
                format: 'LT'
            });
            $('#timepicker9').datetimepicker({
                format: 'LT'
            });
            $('#timepicker10').datetimepicker({
                format: 'LT'
            });
            $('#timepicker11').datetimepicker({
                format: 'LT'
            });
            $('#timepicker12').datetimepicker({
                format: 'LT'
            });
            $('#timepicker13').datetimepicker({
                format: 'LT'
            });
            $('#timepicker14').datetimepicker({
                format: 'LT'
            });
        }); 
        function generarTabla(){
          var tabla = $('#tabladt').editableTableWidget();
          $("#tabladt tbody tr td").change(function(){
          if(/number/.test($(this).attr("class"))){
            if(!/^\d*$/.test($(this).text()))
              $(this).text("");
          }
          generarTabla();
          }).click(function(){
            var celda=$(this);
            celdaActiva=celda;
            if(/docente/.test(celda.attr("class"))){
              $("#modalProfesores").modal("show");
            }
            if(/dias/.test(celda.attr("class"))){
              $("#modalDias").modal("show");
            }
            if(/horario/.test(celda.attr("class"))){
              var dias=celda.parent().children().eq(6).text();
              dias=dias.split(" - ");
              $(".lunes").hide(0);
              $(".martes").hide(0);
              $(".miercoles").hide(0);
              $(".jueves").hide(0);
              $(".viernes").hide(0);
              $(".sabado").hide(0);
              $(".domingo").hide(0);
              for(var i=0;i<dias.length;i++){
                $("."+dias[i].toLowerCase()).show(0);
              }
              $("#modalHorario").modal("show");
            }
            if(/fecha/.test(celda.attr("class"))){
              $("#modalFecha").modal("show");
            }
          });
          var filas = $("#tabladt tbody tr");
          var rotaciones=[];
          var cont=0;
          filas.each(function(){
            rotaciones[cont]=[];
            for(var i=0;i<11;i++)
              rotaciones[cont][i]=$(this).children().eq(i).text();
            cont++;
          });
          $("#rotaciones").val(JSON.stringify(rotaciones));
        }
        function seleccionarProfesor(){
          var profesor = $("#docente_orientador");
          celdaActiva.text(profesor.val()+" - "+$("#docente_orientador option:selected").text());
          $("#modalProfesores").modal("hide");
          generarTabla();
        }
        function seleccionarDias(){
          var dias="";
          $("#selectDias option:selected").each(function(){
            dias=dias+" - "+$(this).val();            
          });
          celdaActiva.text(dias.substr(3));
          $("#modalDias").modal("hide");
          generarTabla();
        }
        function seleccionarFecha(){          
          celdaActiva.text($("#fechaInput").val());
          $("#modalFecha").modal("hide");
          generarTabla();
        }
        function seleccionarHorario(){          
          var dias=celdaActiva.parent().children().eq(6).text();
          dias=dias.split(" - ");
          var texto="";
          for(var i=0;i<dias.length;i++){
            texto=texto+" - "+dias[i]+": ";
            switch(dias[i]){
              case "Lunes":
                texto=texto+$("#timepicker1").val()+" a ";
                texto=texto+$("#timepicker2").val();
                break;
              case "Martes":
                texto=texto+$("#timepicker3").val()+" a ";
                texto=texto+$("#timepicker4").val();
                break;
              case "Miercoles":
                texto=texto+$("#timepicker5").val()+" a ";
                texto=texto+$("#timepicker6").val();
                break;
              case "Jueves":
                texto=texto+$("#timepicker7").val()+" a ";
                texto=texto+$("#timepicker8").val();
                break;
              case "Viernes":
                texto=texto+$("#timepicker9").val()+" a ";
                texto=texto+$("#timepicker10").val();
                break;
              case "Sabado":
                texto=texto+$("#timepicker11").val()+" a ";
                texto=texto+$("#timepicker12").val();
                break;
              case "Domingo":
                texto=texto+$("#timepicker13").val()+" a ";
                texto=texto+$("#timepicker14").val();
                break;
            }
          }
          celdaActiva.text(texto.substr(3));
          $("#modalHorario").modal("hide");
          generarTabla();
        }
    </script>
@stop