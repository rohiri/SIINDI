@extends('layaouts.newtablas')

@section('content')
   <section class="content">  

   <div class="col-md-3">
    {!! Form::label("Tipo de Grafica") !!}
    {!! Form::select("tipo",[0=>"Encuestas",1=>"Homologaciones"],null,["id"=>"tipo","class"=>"form-control"]) !!}
    </div>
    <div class="col-md-2">
    {!! Form::label("Periodo") !!}
    {!! Form::select("periodo",$periodos,null,["id"=>"periodo","class"=>"form-control"]) !!}
    </div>
    <div class="col-md-6">
    {!! Form::label("Curso") !!}
    {!! Form::select("curso",$cursos,null,["id"=>"curso","class"=>"form-control"]) !!}
    </div>
    <br> <br> <br> <br>
    
    
     <button class="btn btn-success" type="button" onclick="graficar();">Graficar</button>
     <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    <br><br>
  </section>
@endsection
@section("js-adicional")      
  <script type="text/javascript" src="{!! URL('js/highcharts.js') !!}"></script>
   <script type="text/javascript">       
       function graficarHomologacion(data){
         $('#container').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: data.titulo
            },
            xAxis: {
                categories: data.cats,
                title: {
                  text: data.tituloX
              }
            },
            yAxis: {
                title: {
                    text: data.tituloY
                }
            },
            series: [{
                name: data.nombreSerie1,
                data: data.serie1
            },{
                name: data.nombreSerie2,
                data: data.serie2
            }]
        });
       }
       function graficarEncuesta(data){
        $('#container').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: data.titulo
            },
            xAxis: {
                categories: data.cats,
                title: {
                  text: data.tituloX
              }
            },
            yAxis: {
                title: {
                    text: data.tituloY
                }
            },
            series: data.series
        }); 
       }
       function graficar(){
          var tipo=$("#tipo").val();
          var periodo=$("#periodo").val();
          var curso=$("#curso").val();
          $.get("{!! URL("graficar") !!}",{tipo:tipo,periodo:periodo,curso:curso},function(data){
            if(tipo==1)
              graficarHomologacion(data);
            else
              graficarEncuesta(data);
          });
       }
   </script>
@stop