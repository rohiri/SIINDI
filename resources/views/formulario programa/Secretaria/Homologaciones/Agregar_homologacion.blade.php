@extends('layaouts.newtablas')
@section('content')

    <section class="content">
     <div class="modal fade" id="modalCursos">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Seleccione el curso</h4>
            </div>
            <div class="modal-body">
              {!! Form::select("curso",$cursos,null,["class"=>"form-control","id"=>"curso"]) !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="seleccionar();">Seleccionar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <div class="row"> 
      <div class="col-lg-12">
      
          <div class="box">
            <div class="box-header" data-original-title>
              <h2><i class="fa fa-files-o"></i><span class="break"></span><b>Datos Generales Homologaciones</b></h2>
              <div class="box-icon">
              
                <a href="#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                
              </div>
            </div>

            <div class="box-content">
                  {!!Form::open($route) !!}  
                  <div class="form-group">        

             {!! Form::label('Fecha')!!} 
            {!!Form::date('fecha',$homologacion->fecha,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}

            {!! Form::label('Estudiante')!!}       
            {!! Form::select("estudiante",$estudiantes,$homologacion->id_estudiante,["class"=>"form-control"]) !!}

            {!! Form::label('Periodo')!!}       
            {!! Form::select("periodo",$periodos,$homologacion->id_periodo,["class"=>"form-control"]) !!}             

            {!!Form::label('Programa origen')!!}<br>
            {!!Form::text('programa_origen',$homologacion->programa_origen,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}

            {!!Form::label('Programa destino')!!}<br>
            {!!Form::select('programa_destino',$programas,$homologacion->programa_destino,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}
            {!!Form::label('Interna')!!}<br>
            <input type="checkbox" id="tipo" name="tipo" {!! $homologacion->interna !!} onclick="cambiarTipo();">
            <div id="externa">
            {!!Form::label('Universidad origen')!!}<br>
            {!!Form::text('UNIVERSIDAD',$homologacion->universidad_origen,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}
            </div>
            <div id="interna">
            {!!Form::label('Plan origen')!!}<br>
            {!!Form::text('plan_origen',$homologacion->plan_origen,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}

            {!!Form::label('Plan destino')!!}<br>
            {!!Form::select('plan_destino',$planes,$homologacion->plan_destino,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}
            </div>
                   
           
                  </div>
                 


            <div class="box-header" data-original-title>
              <h2><i class="fa fa-files-o"></i><span class="break"></span><b>Datos Universidad</b></h2>
              <div class="box-icon">
                
                <a href="#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                
              </div>
            </div>
            <div class="box-content">
              <table class="table table-striped table-bordered bootstrap-datatable datatable" id="tabla">
                <thead>
                  <tr>
                    <th>Semestre Origen</th>
                    <th>Curso Origen</th>
                    <th>Código</th>
                    <th>Créditos</th>
                    <th>Nota Origen</th>
                    <th>Curso Destino</th>
                    <th>Nota Destino</th>
                    <th></th>
                    
                  </tr>
                </thead>   
                <tbody>
                  @foreach($detalle as $d)
                    <tr>
                      <td class="">{!! $d->semestre_origen !!}</td>
                      <td class="">{!! $d->curso_origen !!}</td>
                      <td class="">{!! $d->codigo_curso_origen !!}</td>
                      <td class="">{!! $d->creditos_origen !!}</td>
                      <td class="">{!! $d->nota_origen !!}</td>
                      <td class="no-edit curso">{!! $d->id_curso_destino." - ".$d->nombre_curso !!}</td>
                      <td class="">{!! $d->nota_destino !!}</td>
                      <td class="no-edit"><button type="button" class="btn btn-danger" onclick="quitarFila(this);"><i class="fa fa-times"></i></button></td>
                    </tr>
                  @endforeach
                </tbody>
                
                
              </table> 
              <button type="button" class="btn btn-success" onclick="agregarFila();"><i class="fa fa-plus"></i> Agregar Curso</button>
              
              
                  
                         
            </div>                          
              
             <div class="form-group">
            {!!Form::submit('Guardar',['class'=>'btn btn-success'])!!}
                  </div>

                         
            </div>
         


         
              <input type="hidden" name="preg" id="preg">
          
                  {!! Form::close() !!}
                         
            </div>      
          
          
        
      
      </div><!--/row-->
    </div>

  </section><!-- /.content -->
  
@endsection
@section('js-adicional')
    <script>
    var celdaActiva;
        $(document).ready(function(){
            generarTabla();
            $("#externa").hide();
        }); 
        function agregarFila(){
          var cantidad = $("#tabla tbody tr").size();
          $("#tabla tbody").append('<tr><td></td><td></td><td></td><td></td><td></td><td class="no-edit curso"></td><td></td><td class="no-edit"><button type="button" class="btn btn-danger" onclick="quitarFila(this);"><i class="fa fa-times"></i></button></td></tr>');
          generarTabla();
        }
        function generarTabla(){
          var tabla = $('#tabla').editableTableWidget();
          $("#tabla tbody tr td").change(function(){
            generarTabla();
          }).click(function(){
            var celda=$(this);
            celdaActiva=celda;
            if(/curso/.test(celda.attr("class"))){
              $("#modalCursos").modal("show");
            }
          });
          var filas = $("#tabla tbody tr");
          var tabla=[];
          var cont=0;
          filas.each(function(){
            tabla[cont]=[];
            tabla[cont][0]=$(this).children().eq(0).text();
            tabla[cont][1]=$(this).children().eq(1).text();
            tabla[cont][2]=$(this).children().eq(2).text();
            tabla[cont][3]=$(this).children().eq(3).text();
            tabla[cont][4]=$(this).children().eq(4).text();
            tabla[cont][5]=$(this).children().eq(5).text();
            tabla[cont][6]=$(this).children().eq(6).text();
            cont++;
          });
          $("#preg").val(JSON.stringify(tabla));
        }
        function quitarFila(elemento){
          var fila = $(elemento).parent().parent();
          fila.remove();
        }
        function seleccionar(){
          celdaActiva.text($("#curso option:selected").val()+" - "+$("#curso option:selected").text());
          $("#modalCursos").modal("hide");
          generarTabla();  
        }
        function cambiarTipo(){
          if($("#tipo").is(":checked")){        
            $("#externa").hide(100,function(){
              $("#interna").show(200);
            });
          }else{
            $("#interna").hide(100,function(){
              $("#externa").show(200);
            });
          }
        }
    </script>
@stop