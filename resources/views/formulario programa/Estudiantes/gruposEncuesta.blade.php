@extends('layaouts.newtablas')
@section('content')

<section class="content">
  @if(\Session::get("mensaje"))
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {!! \Session::get("mensaje") !!}
    </div>
  @endif
  <div class="box">
    <div class="box-header">
      <h2><i class="fa fa-pencil-square-o"></i><b>Cursos a Evaluar</b></h2>
    </div>
    <div class="box-content">
      <table class="table" id="preguntas">
        <thead>
          <tr>
            <th>Nombre del Curso</th>
            <th style="width:50px;">Evaluar</th>
          </tr>
        </thead>
        <tbody>
          @foreach($cursos as $p)
            <tr>
              <td>{!! $p->nombre_curso !!}</td>
              @if(\FCS\Encuesta::verificarSiEvaluo($p->id_grupo))
                <td class="no-edit"><a href="{!! URL("encuesta/".$p->id_grupo) !!}" class="btn btn-success"><i class="fa fa-check"></i> Evaluar</a></td>
              @else
                <td class="no-edit"><a href="#" class="btn btn-info"><i class="fa fa-check"></i> Ya evaluado</a></td>
              @endif
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</section>   
@endsection
@section('js-adicional')
    <script>
        
    </script>
@stop