@extends('layaouts.tablas')
  @section('content')
    <section class="content-header">
          <h1><strong>
            SIINDI</strong>
            <small>Version 1.0</small>
          </h1>
        </section>
        <div align="center">
            <img src="{!! URL("assets/logo.png") !!}"><br><br>
            <p style="color: #C20000; font-size: 38px; font-weight: bold;">SISTEMA DE INFORMACIÓN DE INDICADORES DEL CNA</p>
            <p style="color: #C20000; font-size: 24px; font-weight: bold;">Facultad de Ciencias de la Salud</p>
        </div>
@endsection