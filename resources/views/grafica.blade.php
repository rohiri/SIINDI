<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{{ $page_title or "Sistema de Gestion de Indicadores CNA" }}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.4 -->
        {!! Html::style('assets/bootstrap/css/bootstrap.min.css')!!}
        <!-- Font Awesome Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <!-- Theme style -->
        {!!Html::style('assets/dist/css/AdminLTE.min.css')!!}
        {!!Html::style('assets/dist/css/skins/_all-skins.min.css')!!}
    </head>
    <body class="skin-red sidebar-mini">
        <div class="cargando" style="display: block; position:absolute; z-index:99999; top:0; left:0; width:100%; height:100%; background:rgba(0,0,0,0.2); padding-left:50%; padding-top:300px; display:none;"><i class="fa fa-5x fa-spinner fa-pulse"></i></div>
        <div class="wrapper">
            @include('Dashboard.header')
            @include('Dashboard.sidebar')
            <div class="content-wrapper">

                <section class="content">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                        {!!Form::select('Grafica',
                        ['Numero Profesores por Titulacion',
                         'Numero Profesores por Evaluación',
                         'Numero Profesores por Vinculación',
                         'Numero Movilidades por Caracter Evento',
                         'Numero Proyectos por Tipo',
                         'Porcentaje de Responsabilidades Docente',
                         'Número de Horas por Responsabilidad Docente',
                         'Porcentaje de Docentes por Responsabilidad'],'s',
                         ['id'=>'Grafica','class'=>'form-control'])!!} 
                        </div>
                    </div>
                    <div class="col-xs-2">
                            {!!Form::select('Periodo',$periodos,null,['id'=>'Periodo','class'=>'form-control select2'])!!} 
                        </div>
                    <div class="col-xs-2">
                        <button type="button" class="btn btn-success" onclick="gr();">Graficar</button>
                    </div>
                </div>
                  <div class="row">
                    <div class="col-xs-9">
                      <div class="box">
                        <div class="box-header">
                          <h3 class="box-title">Indicadores CNA</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div id="container"></div>
                        </div>                                    
                            </div>
                        </div>
                    </div>
                </section>
                
            </div>
            @include('Dashboard.footer')
        </div>
        {!!Html::script('assets/plugins/jQuery/jQuery-2.1.4.min.js')!!}
        <!-- Bootstrap 3.3.2 JS -->
        {!!Html::script('assets/bootstrap/js/bootstrap.min.js')!!}
        <!-- DATA TABES SCRIPT-->
        {!!Html::script('assets/plugins/datatables/jquery.dataTables.min.js')!!}
        {!!Html::script('assets/plugins/datatables/dataTables.bootstrap.min.js')!!}  
        <!-- SlimScroll -->
        {!!Html::script('assets/plugins/slimScroll/jquery.slimscroll.min.js')!!}
        <!-- FastClick -->
        {!!Html::script('assets/plugins/fastclick/fastclick.min.js')!!}
        <!-- AdminLTE App -->
        {!!Html::script('assets/dist/js/app.min.js')!!}
        <!-- AdminLTE for demo purposes -->
        {!!Html::script('assets/dist/js/demo.js')!!}
        <script src="http://code.highcharts.com/highcharts.js"></script>
        <script>
//quirn recibe
        function pintar(d){
         
         $('#container').highcharts({
                            chart: {
                                type: 'column'
                            },
                            title: {
                            text: d.titulografica
                            },
                            xAxis: {
                                categories: d.datos1,
                                title: {
                                    text: d.tituloX
                                }
                            },
                            yAxis: {
                                allowDecimals: false,
                                title: {
                                    text: d.tituloY
                                }
                            },
                            series: [{
                                name: d.nombreSerie,
                                data: d.datos2
                            }]
                        });
        }
            ///realiaza la peticion
           function gr()
            {
                var valor = $("#Grafica").val();
                var periodo = $("#Periodo").val();
                var url = "{!! URL('grafica') !!}/"+valor;
                $(".cargando").show(0);
                $.get(url,{periodo:periodo},function(data){
                    pintar(data);
                    $(".cargando").hide(0);
                });

            }

        </script>
    </body>
</html>