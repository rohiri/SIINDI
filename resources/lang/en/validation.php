<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'email'                => 'The :attribute must be a valid email address.',
    'filled'               => 'The :attribute field is required.',
    'exists'               => 'The selected :attribute is invalid.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'id_periodo' => [
            'unique'   => 'El Periodo Ya Ha Sido Registrado',
            'required' => 'El Campo Periodo Es Obligatorio',],
        'e_inscritos' => [
            'regex'    => 'El Formato de Estudiantes Inscritos No Es Correcto (Solo valores entre 0-999)',
            'required' => 'El Campo Estudiantes Inscritos Es Obligatorio',],
        'e_admintidos' => [
            'regex'    => 'El Formato de Estudiantes Admitidos No Es Correcto (Solo valores entre 10-999)',
            'required' => 'El Campo Estudiantes Inscritos Es Obligatorio',],
        'matriculados_total' => [
            'regex'    => 'El Formato de Estudiantes Matriculados En Total No Es Correcto (Solo valores entre 10-9999)',
            'required' => 'El Campo Estudiantes Inscritos Es Obligatorio',],
        'matriculados_primersemestre' => [
            'regex'    => 'El Formato de Matriculados En Primer Semestre No Es Correcto (Solo valores entre 10-999)',
            'required' => 'El Campo Estudiantes Inscritos Es Obligatorio',],
        'graduados' => [
            'regex'    => 'El Formato de Estudiantes Graduados No Es Correcto (Solo valores entre 0-9999)',
            'required' => 'El Campo Estudiantes Inscritos Es Obligatorio',],
        'retirados' => [
            'regex'    => 'El Formato de Estudiantes Retirados No Es Correcto (Solo valores entre 0-9999)',
            'required' => 'El Campo Estudiantes Inscritos Es Obligatorio',],
        'tasa_desercion' => [
            'regex'    => 'El Formato de Tasa de Desercion Inscritos No Es Correcto (Solo valores entre 0-99)',
            'required' => 'El Campo Estudiantes Inscritos Es Obligatorio',],
        'tasa_culminacion' => [
            'regex'    => 'El Formato de Tasa de Culminación No Es Correcto (Solo valores entre 0-99)',
            'required' => 'El Campo Estudiantes Inscritos Es Obligatorio',],
        'codigo_curso' => [
            'regex'  => 'El Formato del Código Del Curso No Es Correcto (Deber Ser de 6 Caracteres)',
            'unique' => 'El Código del Curso Ya Ha Sido Registrado',
            'required' => 'El Campo Código del Curso Es Obligatorio',],
        'nombre_curso' => [
            'min' => 'El Nombre del Curso No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Nombre del Curso No Debe Ser Superior a :max Caracteres',
            'required' => 'El Campo Nombre Del Curso Es Obligatorio',],
        'tipo_curso' => [
            'min' => 'El Campo Tipo Del Curso No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Campo Tipo Del Curso No Debe Ser Superior a :max Caracteres',
            'required' => 'El Campo Tipo Del Curso Es Obligatorio',],
        'semestre' => [
            'min' => 'El Campo Semeste No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Campo Semestre No Debe Ser Superior a :max Caracteres',
            'required' => 'El Campo Semestre  Es Obligatorio',],
        'area' => [
            'min' => 'El Campo Area No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Campo Area No Debe Ser Superior a :max Caracteres',
            'required' => 'El Campo Area Es Obligatorio',],
        'creditos' => [
            'min' => 'El Campo Creditos No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Campo Creditos No Debe Ser Superior a :max Caracteres',
            'required' => 'El Campo Creditos Es Obligatorio',],
        'id_planestudio' => [
            'required' => 'El Campo Plan de Estudio Es Obligatorio',],
        'id_profesor' => [
            'required' => 'El Campo Profesor Es Obligatorio',],
        'valor'=>[
            'required'=>'El Campo Valor Es Obligatorio',
            'regex'=>'El Formato Del Campo Valor No Es Correcto (Debe ser de 1 a 2 Caracteres)',],

       'numero_consejo'=>[
            'required'=>'El Número Del Consejo Es Obligatorio',
            'min' => 'El Número Del Consejo No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Número Del Consejo No Debe Ser Superior a :max Caracteres',],
        'fecha'=>[
            'required'=>'El Campo Fecha Es Obligatorio',],
        'nombre_evento'=>[
            'required'=>'El Campo Nombre Del Evento es Obligatorio',
            'min' => 'El Nombre Del Evento No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Nombre Del Evento No Debe Ser Superior a :max Caracteres',],
        'lugar'=>[
            'min' => 'El Lugar No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Lugar No Debe Ser Superior a :max Caracteres',],
        'caracter_evento'=>[
            'required'=>'El Campo Caracter Del Evento es Obligatorio',],
        'id_tipoeventos'=>[
            'required'=>'El Campo Tipo De Evento es Obligatorio',],

        'nombre_actividad'=>[
            'required'=>'El Campo Nombre De La Actividad Es Obligatorio',
            'min' => 'El Campo Nombre De La Actividad No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Campo Nombre De La Actividad No Debe Ser Superior a :max Caracteres',],
        'sesion_consejo'=>[
            'required'=>'El Campo Número De La Sesión Del Consejo Es Obligatorio',
            'min' => 'El Campo Número De La Sesión Del Consejo No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Campo Número De La Sesión Del Consejo No Debe Ser Superior a :max Caracteres',],
        'fecha_sesion'=>[
            'required'=>'La Fecha De La Sesión Es Obligatoria',],

        'tipo_movilidad'=>[
            'required'=>'El Campo Tipo De Movilidad Es Obligatorio',],
        'id_evento'=>[
            'required'=>'El Campo Evento Es Obligatorio'],

        'nombre_nivelestudio'=>[
            'required'=>'El Nombre Del Nivel De Estudio Es Obligatorio',
            'min' => 'El Nombre Del Nivel De Estudio No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Nombre Del Nivel De Estudio No Debe Ser Superior a :max Caracteres',],

        'nombre_programa'=>[
            'required'=>'El Nombre Del Programa Es Obligatorio',
            'min' => 'El Nombre Del Programa No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Nombre Del Programao No Debe Ser Superior a :max Caracteres',],

        'nombre_tipoevento'=>[
            'required'=>'El Nombre Del Tipo De Evento Es Obligatorio',
            'min' => 'El Nombre Del Tipo De Evento No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Nombre Del Tipo De Evento No Debe Ser Superior a :max Caracteres',],

        'nombre_vinculacion'=>[
            'required'=>'El Nombre De La Vinculación Es Obligatorio',
            'min' => 'El Nombre De La Vinculación No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Nombre De La Vinculación No Debe Ser Superior a :max Caracteres',],

        'id_programa'=>[
            'required'=>'El Nombre Del Programa Es Obligatorio',],

        'id_nivelestudio'=>[
            'required'=>'El Nombre Del Nivel De Estudio Es Obligatorio',],

        'titulo_estudio'=>[
            'required'=>'El Título De Estudio Es Obligatorio',
            'min' => 'El Título De Estudio No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Título De Estudio No Debe Ser Superior a :max Caracteres',],

        'cedula'=>[
            'required'=>'El Campo Cédula Es Obligatorio',
            'unique'=>'El Campo Cédula Es Único',
            'regex'=>'El Formato Del Campo Cédula No Es Correcto (Debe ser de 7, 8, o 10 Caracteres)',],

        'primer_nombre'=>[
            'required'=>'El Campo Primer Nombre Es Obligatorio',
            'min' => 'El Campo Primer Nombre No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Campo Primer Nombre No Debe Ser Superior a :max Caracteres',],

        'segundo_nombre'=>[
            'min' => 'El Campo Segundo Nombre No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Campo Segundo Nombre No Debe Ser Superior a :max Caracteres',],

        'primer_apellido'=>[
            'required'=>'El Campo Primer Apellido Es Obligatorio',
            'min' => 'El Campo Primer Apellido  No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Campo Primer Apellido  No Debe Ser Superior a :max Caracteres',],

        'segundo_apellido'=>[
            'min' => 'El Campo Segundo Apellido  No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Campo Segundo Apellido  No Debe Ser Superior a :max Caracteres',],

        'email'=>[
            'required'=>'El Email Es Obligatorio',
            'email'=>'El Formato Del Campo Email No Es Correcto',],
                          
        'telefono'=>[
            'min' => 'El Campo Teléfono No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Campo Teléfono No Debe Ser Superior a :max Caracteres',],


        'codigo'=>[
            'required'=>'El Campo Código Es Obligatorio',
            'unique'=>'El Campo Código Es Único',
            'regex'=>'El Formato Del Código No Es Correcto (Debe ser de 6 Caracteres)'],

        'fecha_resolucion'=>[
            'required'=>'El Campo Fecha De Resolución Es Obligatorio',],

        'numero_resolucion'=>[
            'required'=>'El Campo Número De Resolución Es Obligatorio',
            'min' => 'El Campo Número De Resolución No Debe Ser Inferior a :min Caracteres',
            'max' => 'El Campo Número De Resolución No Debe Ser Superior a :max Caracteres',],

        'fecha_inicio'=>[
            'required'=>'El Campo Fecha De Inicio Es Obligatorio',],

        'fecha_fin'=>[
            'required'=>'El Campo Fecha Fin Es Obligatorio',],
      
        'id_programa'=>[
            'required'=>'El Campo Programa Es Obligatorio',],

    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
