<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FCS\Menu::create([
            'nombre'=>'Eventos',
            'ruta'=>'evento',
            'imagen'=>'fa fa-desktop'
            ]);
        FCS\Menu::create([
            'nombre'=>'Tipos De Evento',
            'ruta'=>'tipo-evento',
            'imagen'=>'fa fa-clipboard'
            ]);
        FCS\Menu::create([
            'nombre'=>'Usuarios',
            'ruta'=>'usuario',
            'imagen'=>'fa fa-user'
            ]);
        FCS\Menu::create([
            'nombre'=>'Tipo De Vinculación',
            'ruta'=>'vinculacion',
            'imagen'=>'fa fa-credit-card'
            ]);
        FCS\Menu::create([
            'nombre'=>'Programas',
            'ruta'=>'programa',
            'imagen'=>'fa fa-hospital-o'
            ]);
        FCS\Menu::create([
            'nombre'=>'Nivel Estudio',
            'ruta'=>'nivel-estudio',
            'imagen'=>'fa fa-graduation-cap'
            ]);
        FCS\Menu::create([
            'nombre'=>'Profesores',
            'ruta'=>'profesores',
            'imagen'=>'fa fa-user-secret'
            ]);
        FCS\Menu::create([
            'nombre'=>'Plan de Estudio',
            'ruta'=>'planes-estudio',
            'imagen'=>'fa fa-newspaper-o'
            ]);
        FCS\Menu::create([
            'nombre'=>'Cursos',
            'ruta'=>'cursos',
            'imagen'=>'fa fa-book'
            ]);
        FCS\Menu::create([
            'nombre'=>'Graficas-Secretaria',
            'ruta'=>'grafica',
            'imagen'=>'fa fa-bar-chart'
            ]);
        FCS\Menu::create([
            'nombre'=>'Titulacion Docente',
            'ruta'=>'titulacion-docente',
            'imagen'=>'fa fa-mortar-board'
            ]);
        FCS\Menu::create([
            'nombre'=>'Evaluacion Docente',
            'ruta'=>'evaluacion-docente',
            'imagen'=>'fa fa-pencil'
            ]);
        FCS\Menu::create([
            'nombre'=>'Responsabilidad Docente',
            'ruta'=>'responsabilidad-docente',
            'imagen'=>'fa fa-calendar'
            ]);
        FCS\Menu::create([
            'nombre'=>'Preu-Estudiante',
            'ruta'=>'secretaria-estudiante',
            'imagen'=>'fa fa-folder-open'
            ]);
        FCS\Menu::create([
            'nombre'=>'Actividades',
            'ruta'=>'actividades',
            'imagen'=>'fa fa-calendar'
            ]);
        FCS\Menu::create([
            'nombre'=>'Funcion Administrativa',
            'ruta'=>'funciones-administrativas',
            'imagen'=>'fa fa-thumb-tack'
            ]);
        FCS\Menu::create([
            'nombre'=>'Movilidades',
            'ruta'=>'movilidad-docente',
            'imagen'=>'fa fa-bus'
            ]);
        FCS\Menu::create([
            'nombre'=>'Estudiantes',
            'ruta'=>'estudiante',
            'imagen'=>'fa fa-user-md'
            ]);
        FCS\Menu::create([
            'nombre'=>'Graficas-Programa',
            'ruta'=>'graficas',
            'imagen'=>'fa fa-bar-chart'
            ]);
        FCS\Menu::create([
            'nombre'=>'Grupos',
            'ruta'=>'grupo',
            'imagen'=>'fa fa-users'
            ]);
        FCS\Menu::create([
            'nombre'=>'Homologaciones',
            'ruta'=>'homologacion',
            'imagen'=>'fa fa-files-o'
            ]);
        FCS\Menu::create([
            'nombre'=>'Inscripciones',
            'ruta'=>'inscripcion',
            'imagen'=>'fa fa-user-plus'
            ]);
        FCS\Menu::create([
            'nombre'=>'Periodos',
            'ruta'=>'periodo',
            'imagen'=>'fa fa-map-marker'
            ]);
        FCS\Menu::create([
            'nombre'=>'Practicas',
            'ruta'=>'practicas',
            'imagen'=>'fa fa-stethoscope'
            ]);
        FCS\Menu::create([
            'nombre'=>'Preguntas',
            'ruta'=>'preguntas',
            'imagen'=>'fa fa-question'
            ]);
        FCS\Menu::create([
            'nombre'=>'Rotaciones',
            'ruta'=>'rotacion',
            'imagen'=>'fa fa-ambulance'
            ]);
        FCS\Menu::create([
            'nombre'=>'Evaluacion Curso',
            'ruta'=>'encuesta',
            'imagen'=>'fa fa-pencil-square-o'
            ]);
    }
}