<?php

use Illuminate\Database\Seeder;

class GruposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FCS\Grupo::create([
            'codigo' => '987654321',
            'id_curso'=>'1',
            'id_periodo'=>'1'
            ]);
        FCS\Grupo::create([
            'codigo' => '887654321',
            'id_curso'=>'2',
            'id_periodo'=>'2'
            ]);
        FCS\Grupo::create([
            'codigo' => '787654321',
            'id_curso'=>'3',
            'id_periodo'=>'1'
            ]);
        FCS\Grupo::create([
            'codigo' => '687654321',
            'id_curso'=>'4',
            'id_periodo'=>'2'
            ]);
        FCS\Grupo::create([
            'codigo' => '587654321',
            'id_curso'=>'5',
            'id_periodo'=>'1'
            ]);
        FCS\Grupo::create([
            'codigo' => '487654321',
            'id_curso'=>'6',
            'id_periodo'=>'2'
            ]);
        FCS\Grupo::create([
            'codigo' => '387654321',
            'id_curso'=>'7',
            'id_periodo'=>'1'
            ]);
        FCS\Grupo::create([
            'codigo' => '887654321',
            'id_curso'=>'1',
            'id_periodo'=>'2'
            ]);
        FCS\Grupo::create([
            'codigo' => '187654321',
            'id_curso'=>'8',
            'id_periodo'=>'2'
            ]);
        FCS\Grupo::create([
            'codigo' => '187654321',
            'id_curso'=>'8',
            'id_periodo'=>'3'
            ]);
    }
}
