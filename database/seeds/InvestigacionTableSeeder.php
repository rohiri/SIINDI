<?php

use Illuminate\Database\Seeder;

class InvestigacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FCS\ResponsabilidadInvestigacion::create([
            'id_profesor'=>'1',
            'id_vinculacion'=>'1',
            'id_periodo'=>'1',
            'id_proyecto'=>'1',
            'horas_semanal'=>'4'
            ]);
    }
}
