<?php

use Illuminate\Database\Seeder;

class PermisosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FCS\Permiso::create([
            'iduser'=>'1',
            'idmenus'=>'3',
            'permiso'=>'1'
            ]);
    }
}
