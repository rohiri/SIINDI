<?php

use Illuminate\Database\Seeder;

class PlanEstudioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FCS\PlanEstudio::create([
            'codigo'=>'500FA',
            'fecha_resolucion'=>'2015-10-10',
            'numero_resolucion'=>'123456',
            'fecha_inicio'=>'2015-10-10',
            'fecha_fin'=>'2018-10-10',
            'id_programa'=>'1'
            ]);
    }
}
