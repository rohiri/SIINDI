<?php

use Illuminate\Database\Seeder;

class NivelEstudioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FCS\NivelEstudio::create([
            'nombre_nivelestudio'=>'Pregrado',
            ]);
        FCS\NivelEstudio::create([
            'nombre_nivelestudio'=>'Postgrado',
            ]);
        FCS\NivelEstudio::create([
            'nombre_nivelestudio'=>'Maestria',
            ]);
        FCS\NivelEstudio::create([
            'nombre_nivelestudio'=>'Doctorado',
            ]);
    }
}
