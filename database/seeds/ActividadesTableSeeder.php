<?php

use Illuminate\Database\Seeder;

class ActividadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FCS\Actividad::create([
            'nombre_actividad'=>'CLAUSTRO'
            ]);
        FCS\Actividad::create([
            'nombre_actividad'=>'GRUPO DE TRABAJO'
            ]);
        FCS\Actividad::create([
            'nombre_actividad'=>'CLAUSTRO DOCENTE'
            ]);
        FCS\Actividad::create([
            'nombre_actividad'=>'PLAN DE MEJORAMIENTO PROGRAMA DE ENFERMERÃA'
            ]);
        FCS\Actividad::create([
            'nombre_actividad'=>'ORGANIZACION TARDES ACADEMICAS FACULTAD CIENCIAS DE LA SALUD'
            ]);
        FCS\Actividad::create([
            'nombre_actividad'=>'PARTICIPACION EN CLAUSTRO DOCENTE'
            ]);
        FCS\Actividad::create([
            'nombre_actividad'=>'ESCRITURA ARTICULO'
            ]);
        FCS\Actividad::create([
            'nombre_actividad'=>'RED DE UNIVERSIDADES CON PROGRAMAS DE SALUD FAMILIAR'
            ]);
        FCS\Actividad::create([
            'nombre_actividad'=>'DOCUMENTO DE ESPECIALIZACION AUDITORIA Y GESTION DE LA CALIDAD EN SALUD'
            ]);
        FCS\Actividad::create([
            'nombre_actividad'=>'PRUEBAS SABER PRO'
            ]);
        FCS\Actividad::create([
            'nombre_actividad'=>'PLAN DE MEJORAMIENTO'
            ]);
        FCS\Actividad::create([
            'nombre_actividad'=>'COORDINACIÓN CURSO PRÁCTICAS EN REGENCIA'
            ]);
        FCS\Actividad::create([
            'nombre_actividad'=>'ELABORACION ARTICULO'
            ]);
        FCS\Actividad::create([
            'nombre_actividad'=>'APOYO PLAN DE MEJORAMIENTO'
            ]);
    }
}
