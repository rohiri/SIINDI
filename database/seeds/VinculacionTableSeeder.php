<?php

use Illuminate\Database\Seeder;

class VinculacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FCS\Vinculacion::create([
            'nombre_vinculacion'=>'CATEDRA'
            ]);
        FCS\Vinculacion::create([
            'nombre_vinculacion'=>'OCASIONAL TC'
            ]);
        FCS\Vinculacion::create([
            'nombre_vinculacion'=>'PLANTA'
            ]);
        FCS\Vinculacion::create([
            'nombre_vinculacion'=>'Ocasional Medio Tiempo'
            ]);
        FCS\Vinculacion::create([
            'nombre_vinculacion'=>'Catedrático'
            ]);
    }
}
