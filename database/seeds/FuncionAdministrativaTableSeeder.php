<?php

use Illuminate\Database\Seeder;

class FuncionAdministrativaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'COORDINACIÓN DE CURSO DE ÁREA PROFESIONAL DEL PROG DE ENFERMERÍA'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'DIRECCIÓN DE PROGRAMA ACADÉMICO EN PROCESO DE AUTOEVALUACIÓN O ACREDITACIÓN'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'DIRECCIÓN DE PROGRAMA ACADÉMICO DE GRADO'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'PARTICIPACIÓN EN LAS ACT. DE EDU. CONTINUA, EN EL MARCO DE RELACIÓN DOC-SERV. DEL PROG DE ENFERMERÍA'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'COORDINACIÓN DE LA PRÁCTICA FORMATIVA DEL PROG DE ENFERMERÍA'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'PARTICIPACIÓN EN EL COMITÉ DE PRÁCTICAS FORMATIVAS, CREADO POR EL CONSEJO DE FCS'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'COMITÉ DE EVALUACIÓN Y PROMOCIÓN DOCENTE'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'COORDINACIÓN DE LAS ACTIVIDADES RELACIÓN DOCENCIA - SERVICIO'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'REPRESENTACIÓN ANTE COMITÉS INTER-INSTITUCIONALES, GREMIAL LOCAL, REGIONAL O NACIONAL'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'DIRECCIÓN DE ESCUELA O DEPARTAMENTO'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'DIRECCIÓN DE PROGRAMA ACADÉMICO POSGRADO'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'CONSEJO DE FACULTAD'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'OTROS CENTROS (DEFINIDOS EN EL ACUERDO 012 DE 2009)'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'SECRETARÍA ACADÉMICA DE FACULTAD'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'COORDINACIÓN DE GRUPO DE INVESTIGACIÓN CLASIFICADO POR COLCIENCIAS'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'DIRECCIÓN DE CENTROS DE INVESTIGACIONES O DE PROYECCIÓN SOCIAL'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'PARTICIPANTE EN GRUPO DE ESTUDIO ACTIVO, APROBADO POR EL CONSEJO DE INVESTIGACIONES'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'SECRETARIA TECNICA DE AUTOEVALUACIÓN Y ACREDITACIÓN'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'COMITÉ DE PROGRAMA ACADÉMICO DE GRADO'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'COMITÉ EDITORIAL CREADO POR RESOLUCIÓN RECTORAL'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'COMITÉ DE PUBLICACIONES'
            ]);
        FCS\FuncionAdministrativa::create([
            'nombre_funcion'=>'DIRECCIÓN DE CENTROS DE INVESTIGACIONES O DE PROYECCIÓN SOCIAL'
            ]);
        }
}

