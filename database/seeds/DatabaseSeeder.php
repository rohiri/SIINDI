<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(UserTableSeeder::class);
        $this->call(MenuTableSeeder::class);
        $this->call(PermisosTableSeeder::class);
        $this->call(TipoEventosTableSeeder::class);
        $this->call(ProgramasTableSeeder::class);
        $this->call(PeriodoTableSeeder::class);
        $this->call(NivelEstudioTableSeeder::class);
        $this->call(VinculacionTableSeeder::class);
        $this->call(ProfesoresTableSeeder::class);
        $this->call(PlanEstudioTableSeeder::class);
        $this->call(CursosTableSeeder::class);
        $this->call(GruposTableSeeder::class);
        $this->call(ProyectosTableSeeder::class);
        $this->call(ActividadesTableSeeder::class);
        $this->call(FuncionAdministrativaTableSeeder::class);
        Model::reguard();
    }
}
