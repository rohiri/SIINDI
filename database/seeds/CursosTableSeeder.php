<?php

use Illuminate\Database\Seeder;

class CursosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FCS\Curso::create([
            'codigo_curso'=>'302101',
            'nombre_curso'=>'FUNDAMENTOS DE BIOQUÍMICA I',
            'tipo_curso'=>'Teorico',
            'semestre'=>'I',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302102',
            'nombre_curso'=>'MORFOFISIOLOGÍA',
            'tipo_curso'=>'Teorico',
            'semestre'=>'I',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302103',
            'nombre_curso'=>'SOCIOANTROPOLOGÍA',
            'tipo_curso'=>'Teorico',
            'semestre'=>'I',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302105',
            'nombre_curso'=>'PROCESOS COMUNICATIVOS',
            'tipo_curso'=>'Teorico',
            'semestre'=>'I',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302106',
            'nombre_curso'=>'HISTORIA Y DESARROLLO DE ENFERMERÍA',
            'tipo_curso'=>'Teorico',
            'semestre'=>'I',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302201',
            'nombre_curso'=>'FUNDAMENTOS DE BIOQUÍMICA II',
            'tipo_curso'=>'Teorico',
            'semestre'=>'II',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302202',
            'nombre_curso'=>'MICRIOBIOLOGÍA Y PARASITOLOGÍA',
            'tipo_curso'=>'Teorico',
            'semestre'=>'II',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302203',
            'nombre_curso'=>'FISIOPATOLOGÍA',
            'tipo_curso'=>'Teorico',
            'semestre'=>'II',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302204',
            'nombre_curso'=>'FUNDAMENTOS Y TÉCNICAS PARA EL CUIDADO I',
            'tipo_curso'=>'Teorico',
            'semestre'=>'II',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302301',
            'nombre_curso'=>'FUNDAMENTOS ÉTICOS Y BIOÉTICOS',
            'tipo_curso'=>'Teorico',
            'semestre'=>'II',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302302',
            'nombre_curso'=>'FARMACOLOGÍA',
            'tipo_curso'=>'Teorico',
            'semestre'=>'II',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302303',
            'nombre_curso'=>'CIENCIA, TECNOLOGÍA Y DESARROLLO',
            'tipo_curso'=>'Teorico',
            'semestre'=>'II',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302304',
            'nombre_curso'=>'PSICOLOGÍA Y DESARROLLO HUMANO',
            'tipo_curso'=>'Teorico',
            'semestre'=>'II',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302305',
            'nombre_curso'=>'FUNDAMENTOS Y TÉCNICAS PARA EL CUIDADO II',
            'tipo_curso'=>'Teorico',
            'semestre'=>'II',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302401',
            'nombre_curso'=>'BIOESTADÍSTICA',
            'tipo_curso'=>'Teorico',
            'semestre'=>'II',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302402',
            'nombre_curso'=>'ECOBIOLOGÍA DE POBLACIONES',
            'tipo_curso'=>'Teorico',
            'semestre'=>'II',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302403',
            'nombre_curso'=>'PROMOCIÓN DE LA SALUD Y PREVENCIÓN DE LA ENFERMEDAD',
            'tipo_curso'=>'Teorico',
            'semestre'=>'II',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302404',
            'nombre_curso'=>'CUIDADO DESARROLLO PRENATAL, NACIMIENTO Y RECIÉN NACIDO',
            'tipo_curso'=>'Teorico',
            'semestre'=>'II',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302405',
            'nombre_curso'=>'SISTEMA GENERAL DE SEGURIDAD SOCIAL',
            'tipo_curso'=>'Teorico',
            'semestre'=>'II',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302406',
            'nombre_curso'=>'CÁTEDRA ORINOQUIA',
            'tipo_curso'=>'Teorico',
            'semestre'=>'II',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302501',
            'nombre_curso'=>'EPIDEMIOLOGÍA',
            'tipo_curso'=>'Teorico',
            'semestre'=>'II',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302502',
            'nombre_curso'=>'CUIDADO DE LA SALUD DE COLECTIVOS I',
            'tipo_curso'=>'Teorico',
            'semestre'=>'II',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302503',
            'nombre_curso'=>'CUIDADO DE LA SALUD AL NIÑO',
            'tipo_curso'=>'Teorico',
            'semestre'=>'II',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
        FCS\Curso::create([
            'codigo_curso'=>'302504',
            'nombre_curso'=>'TENDENCIAS EN ENFERMERÍA',
            'tipo_curso'=>'Teorico',
            'semestre'=>'II',
            'area'=>'PS',
            'creditos'=>'4',
            'id_planestudio'=>'1'
            ]);
    }
}
