<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PracticaFormativa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('practica_formativas', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_grupo')->unsigned();
            $table->string('nombre_practica',100);
            $table->string('escenario_practica',100);
            $table->string('competencias_curso',500);
            $table->string('objetivo_practica',500);
            $table->double('valor_totalpractica',15,8);
            $table->string('docente_coordinador',100);
            $table->foreign('id_grupo')
                  ->references('id')
                  ->on('grupos')
                  ->onUpdate('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('practica_formativas');
    }
}
