<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Respuestas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuestas', function(Blueprint $table){
            $table->increments('idRespuesta');
            $table->integer('pregunta');
            $table->integer('siempre')->default(0);
            $table->integer('casisiempre')->default(0);
            $table->integer('nunca')->default(0);
            $table->integer('algunasveces')->default(0);
            $table->integer('encuesta');
            $table->foreign('pregunta')
                  ->references('idPregunta')
                  ->on('preguntas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('respuestas');
    }
}
