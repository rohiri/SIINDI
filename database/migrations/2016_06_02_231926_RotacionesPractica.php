<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RotacionesPractica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rotaciones_practica', function(Blueprint $table){
            $table->increments('id');
            $table->integer('practica')->unsigned();
            $table->text('actividades');
            $table->integer('numero_estudiantes')->unsigned();
            $table->text('dias');
            $table->text('horario');
            $table->date('fecha_inicio');
            $table->date('fecha_culminacion');
            $table->double('valor', 15, 8);
            $table->integer('docente_orientador')->unsigned();
            $table->integer('rotacion')->unsigned();
            $table->text('ubicacion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rotaciones_practica');
    }
}
