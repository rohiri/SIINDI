<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Proyecto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyecto', function(Blueprint $table){
            $table->increments('id');
            $table->string('codigo',100);
            $table->string('nombre_proyecto',250);
            $table->enum('tipo', array('Investigacion', 'Proyeccion'));
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_terminacion')->nullable();
            $table->string('sesion_consejo',100)->nullable();
            $table->date('fecha_sesion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proyecto');
    }
    
}
