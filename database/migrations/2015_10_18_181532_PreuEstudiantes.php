<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PreuEstudiantes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudiantes_preu', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_periodo')->unsigned();
            $table->integer('e_inscritos')->unsigned()->nullable();
            $table->integer('e_admintidos')->unsigned()->nullable();
            $table->integer('matriculados_total')->unsigned()->nullable();
            $table->integer('matriculados_primersemestre')->unsigned()->nullable();
            $table->integer('graduados')->unsigned()->nullable();
            $table->integer('retirados')->unsigned()->nullable();
            $table->float('tasa_desercion')->nullable();
            $table->float('tasa_culminacion')->nullable();
            $table->foreign('id_periodo')
                  ->references('id')
                  ->on('periodos')
                  ->onUpdate('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('estudiantes_preu');
    }
}
