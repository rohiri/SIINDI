<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EvaluacionPracticaFormativa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluacion_practicaformativa', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_estudiante')->unsigned()->unique();
            $table->integer('id_rotacion')->unsigned()->unique();
            $table->text('indicador');
            $table->string('puntaje',100);
            $table->text('observaciones_especificas');
            $table->string('puntaje_dimension',100);
            $table->string('dimension',100);
            $table->string('puntaje_total',100);
            $table->string('nota',100);
            $table->text('observaciones_generales');
            $table->integer('id_practica')->unsigned()->unique();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('evaluacion_practicaformativa');
    }
}
