<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DetalleHomologacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_homologaciones', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_homologacion')->unsigned()->unique();
            $table->string('semestre_origen',10);
            $table->string('curso_origen',100);
            $table->integer('id_curso_destino')->unsigned();
            $table->string('codigo_curso_origen',100);
            $table->integer('creditos_origen')->unsigned();
            $table->double('nota_origen', 15, 8);
            $table->double('nota_destino', 15, 8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detalle_homologaciones');
    }
}
