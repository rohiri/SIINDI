<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GrupoPractica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupos_practica', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_practica')->unsigned();
            $table->integer('id_estudiante')->unsigned();
            $table->string('grupo',1);
            $table->foreign('id_practica')
                  ->references('id')
                  ->on('practica_formativas')
                  ->onUpdate('CASCADE');
            $table->foreign('id_estudiante')
                  ->references('id')
                  ->on('estudiantes')
                  ->onUpdate('CASCADE');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('grupos_practica');
    }
}
