<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Homologacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homologaciones', function(Blueprint $table){
            $table->increments('id');
            $table->date('fecha');
            $table->string('universidad_origen',250);
            $table->string('programa_origen',200);
            $table->string('plan_origen',200);
            $table->string('interna',100);
            $table->integer('id_periodo')->unsigned();
            $table->integer('id_estudiante')->unsigned();
            $table->integer('programa_destino')->unsigned();
            $table->integer('plan_destino')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('homologaciones');
    }
}
