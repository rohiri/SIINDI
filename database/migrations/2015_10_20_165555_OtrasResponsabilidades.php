<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OtrasResponsabilidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otras_responsabilidades', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_profesor')->unsigned();
            $table->integer('id_vinculacion')->unsigned();
            $table->integer('id_periodo')->unsigned();
            $table->integer('id_actividad')->unsigned();
            $table->float('horas_semanal');
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_terminacion')->nullable();
            $table->date('fecha_entregainforme')->nullable();
            $table->foreign('id_profesor')
                  ->references('id')
                  ->on('profesores')
                  ->onUpdate('CASCADE');
            $table->foreign('id_vinculacion')
                  ->references('id')
                  ->on('vinculaciones')
                  ->onUpdate('CASCADE');
            $table->foreign('id_periodo')
                  ->references('id')
                  ->on('periodos')
                  ->onUpdate('CASCADE');
            $table->foreign('id_actividad')
                  ->references('id')
                  ->on('actividades')
                  ->onUpdate('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('otras_responsabilidades');
    }
}
